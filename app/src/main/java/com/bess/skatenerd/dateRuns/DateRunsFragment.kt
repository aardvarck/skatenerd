package com.bess.skatenerd

import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.widget.Button
import androidx.navigation.findNavController
import com.bess.skatenerd.db.DBCtrl

class DateRunsFragment : Fragment() {
	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		val view = inflater.inflate(R.layout.fragment_dateruns_list, container, false)

		val args = DateRunsFragmentArgs.fromBundle(requireArguments())
		val courseId = args.courseId

		val lst = DBCtrl.get().getAllCourseDateRuns(courseId)
		val crs = DBCtrl.get().getCourseInfo(courseId)
		val countCones = PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean("pref_cones_def", false)

		val dateRunsLst = mutableListOf<DateRunsRecyclerViewAdapter.DateRunsItem>()
		val newDatesFirst = lst.keys.sortedDescending()
		for (dv in newDatesFirst) {
			val rList = lst.getValue(dv)
			val pen = if (countCones) crs.penVal() else 0.0f
			val best = rList.minByOrNull { it.rawTime(pen) }
			dateRunsLst.add(DateRunsRecyclerViewAdapter.DateRunsItem(dv, best!!, rList.count()))
		}

		val recView = view.findViewById<RecyclerView>(R.id.datesList)
		with(recView) {
			layoutManager = GridLayoutManager(context, 3)
			val headers = listOf(
					getString(R.string.hist_head_date),
					getString(R.string.hist_head_best),
					getString(R.string.hist_head_count)
			)
			adapter = DateRunsRecyclerViewAdapter(
					dateRunsLst,
					courseId,
					countCones,
					crs.penVal(),
					headers
			)
		}

		val calBtn = view.findViewById<Button>(R.id.calendarButton)
		calBtn.setOnClickListener {
			view.findNavController()
					.navigate(
							DateRunsFragmentDirections.actionDateRunsFragmentToCalendarFragment(
									courseId
							)
					)
		}

		return view
	}
}

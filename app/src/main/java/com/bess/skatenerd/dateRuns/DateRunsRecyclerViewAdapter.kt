package com.bess.skatenerd

import android.graphics.Typeface
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.databinding.FragmentDaterunsBinding
import com.bess.skatenerd.dbModel.Run
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

class DateRunsRecyclerViewAdapter(
		private val mValues: List<DateRunsItem>,
		private val mCourseId: Int,
		private val countCones: Boolean,
		private val pen: Float,
		private val headers: List<String>
) : RecyclerView.Adapter<DateRunsRecyclerViewAdapter.ViewHolder>() {

	private val formatter = DateTimeFormatter.ofPattern(" dd LLLL yyyy")

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val binding = FragmentDaterunsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
		return ViewHolder(binding)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val runPos = position / 3
		val item = if (runPos == 0) null else mValues[runPos - 1]
		holder.mIdView.text = when (position % 3) {
			0 -> if (runPos == 0) headers[0] else item!!.date.format(formatter)
			1 -> if (runPos == 0) headers[1] else item!!.best.toString(
					countCones,
					pen,
					if (item.best.hasThPart()) 1 else 0
			)
			else -> if (runPos == 0) headers[2] else item!!.runsCount.toString()
		}
		if (runPos == 0) holder.mIdView.setTypeface(null, Typeface.BOLD)
		holder.binding.root.tag = item
	}

	override fun getItemCount(): Int = (mValues.size + 1) * 3

	inner class ViewHolder(val binding: FragmentDaterunsBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
		val mIdView: TextView = binding.gridtext

		init {
			binding.root.setOnClickListener(this)
		}

		override fun onClick(v: View) {
			if (v.tag != null) {
				// TODO bad style..
				ShowCourseFragment.printThPart =
						PreferenceManager.getDefaultSharedPreferences(v.context)
								.getBoolean("pref_show_micro_digit", false)
				v.findNavController().navigate(
						DateRunsFragmentDirections.actionDateRunsFragmentToShowCourseFragment(
								mCourseId, (v.tag as DateRunsItem).date
						)
				)
			}
		}
	}

	data class DateRunsItem(val date: LocalDate, val best: Run, val runsCount: Int)
}

package com.bess.skatenerd

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.databinding.FragmentDayshistoryElementBinding
import com.bess.skatenerd.dbModel.Run
import org.threeten.bp.LocalDate

class DaysHistoryRecyclerNoteViewAdapter(
		private val mValues: List<DaysHistoryFragment.HistEntryInfo>
) : RecyclerView.Adapter<DaysHistoryRecyclerNoteViewAdapter.ViewHolder>() {

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val binding = FragmentDayshistoryElementBinding.inflate(LayoutInflater.from(parent.context), parent, false)
		return ViewHolder(binding)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val dci = mValues[position] as DaysHistoryFragment.DaysNotesInfo
		holder.mIdView.text = dci.note
		holder.mIdView.gravity = Gravity.START
	}

	override fun getItemCount(): Int = mValues.size

	inner class ViewHolder(val binding: FragmentDayshistoryElementBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
		val mIdView: TextView = binding.gridtext

		init {
			binding.root.setOnClickListener(this)
		}

		override fun onClick(v: View) {
//			if( v.tag != null) v.findNavController().navigate(
//							DateRunsFragmentDirections.actionDateRunsFragmentToShowCourseFragment(
//								mCourseId, (v.tag as DateRunsItem).date))
		}
	}

	data class DateRunsItem(val date: LocalDate, val best: Run, val runsCount: Int)
}

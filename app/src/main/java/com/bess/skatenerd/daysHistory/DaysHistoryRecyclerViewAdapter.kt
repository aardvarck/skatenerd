package com.bess.skatenerd

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.databinding.FragmentDayshistorySpotBinding
import com.bess.skatenerd.dbModel.Run
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle

class DaysHistoryRecyclerViewAdapter(
		private val mMode: Int,
		private val mDaysHistory: List<DaysHistoryFragment.DaysSpotInfo>
) : RecyclerView.Adapter<DaysHistoryRecyclerViewAdapter.ViewHolder>() {

	private val formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val binding = FragmentDayshistorySpotBinding.inflate(LayoutInflater.from(parent.context), parent, false)
		return ViewHolder(binding)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val dateSpot = mDaysHistory[position]
		holder.mHeader.text = formatter.format(dateSpot.date) + ", " + dateSpot.spot.name
		holder.mHeader.setTypeface(null, Typeface.BOLD)

		val recView = holder.mList
		with(recView) {
			when (mMode) {
				0 -> {
					layoutManager = GridLayoutManager(context, if (mMode == 0) 2 else 1)
					adapter = DaysHistoryRecyclerCourseViewAdapter(dateSpot.cours)
				}
				else -> {
					layoutManager = GridLayoutManager(context, if (mMode == 0) 2 else 1)
					adapter = DaysHistoryRecyclerNoteViewAdapter(dateSpot.cours)
				}
			}
		}
	}

	override fun getItemCount(): Int = mDaysHistory.size

	inner class ViewHolder(val binding: FragmentDayshistorySpotBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
		val mHeader: TextView = binding.dateSpot
		val mList: RecyclerView = binding.coursesList

		init {
			binding.root.setOnClickListener(this)
		}

		override fun onClick(v: View) {
//			if( v.tag != null) v.findNavController().navigate(
//							DateRunsFragmentDirections.actionDateRunsFragmentToShowCourseFragment(
//								mCourseId, (v.tag as DateRunsItem).date))
		}
	}

	data class DateRunsItem(val date: LocalDate, val best: Run, val runsCount: Int)
}

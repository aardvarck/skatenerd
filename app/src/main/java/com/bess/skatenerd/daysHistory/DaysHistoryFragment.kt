package com.bess.skatenerd

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.Spot
import org.threeten.bp.LocalDate

class DaysHistoryFragment : Fragment() {
	interface HistEntryInfo
	data class DaysCourseInfo(val name: String, val runs: String) : HistEntryInfo
	data class DaysNotesInfo(val note: String) : HistEntryInfo

	data class DaysSpotInfo(val date: LocalDate, val spot: Spot, val cours: List<HistEntryInfo>)

	private var mMode: Int = 0

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		val args = DaysHistoryFragmentArgs.fromBundle(requireArguments())
		mMode = args.historyMode

		val view = inflater.inflate(R.layout.fragment_dayshistory_list, container, false)

		val dayRunsMap = mutableMapOf<LocalDate, MutableMap<Spot, MutableList<HistEntryInfo>>>()

		val sc = DBCtrl.get().getAllSpotsCourses()
		for (sp in sc) {
			if (sp.courses == null) continue
			for (cs in sp.courses!!) {
				if (mMode == 0) {
					val dateRuns = DBCtrl.get().getAllCourseDateRuns(cs.id)
					for ((ld, cr) in dateRuns) {
						if (!dayRunsMap.containsKey(ld)) dayRunsMap[ld] =
								mutableMapOf<Spot, MutableList<HistEntryInfo>>()

						val daysSpotMap = dayRunsMap[ld]!!
						if (!daysSpotMap.containsKey(sp.spot)) daysSpotMap[sp.spot!!] =
								mutableListOf()

						val daySpotCourses = daysSpotMap[sp.spot]!!

						val dayCourseInfo =
								DaysCourseInfo(cs.name, getString(R.string.runs_count, cr.count()))
						daySpotCourses.add(dayCourseInfo)
					}
				} else {
					val courseLabels =
							DBCtrl.get().getCourseLabels(cs.id).sortedByDescending { it.created }
					for (cl in courseLabels) {
						val ld = cl.created.toLocalDate()
						if (!dayRunsMap.containsKey(ld)) dayRunsMap[ld] =
								mutableMapOf<Spot, MutableList<HistEntryInfo>>()

						val daysSpotMap = dayRunsMap[ld]!!
						if (!daysSpotMap.containsKey(sp.spot)) daysSpotMap[sp.spot!!] =
								mutableListOf()

						val daySpotCourses = daysSpotMap[sp.spot]!!

						val dayNotesInfo = DaysNotesInfo(cs.name + " - " + cl.data)
						daySpotCourses.add(dayNotesInfo)
					}
				}
			}
		}
		val days = dayRunsMap.keys.sortedDescending()
		val daysSpots = mutableListOf<DaysSpotInfo>()

		for (d in days) {
			val spots = dayRunsMap[d]!!.keys.sortedBy { it.name }
			for (s in spots) daysSpots.add(DaysSpotInfo(d, s, dayRunsMap[d]!![s]!!))
		}

		val recView = view.findViewById<RecyclerView>(R.id.daysList)
		with(recView) {
			adapter = DaysHistoryRecyclerViewAdapter(mMode, daysSpots)
		}
		return view
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		val headerView = view.findViewById<TextView>(R.id.historyTextView)
		headerView.text =
				if (mMode == 0) getString(R.string.sessions) else getString(R.string.notes)
	}
}

package com.bess.skatenerd

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bess.skatenerd.databinding.FragmentSpotsListBinding
import com.bess.skatenerd.databinding.FragmentStartuploginBinding
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.Course
import com.bess.skatenerd.dbModel.Place
import com.bess.skatenerd.dbModel.Spot
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView
import uk.co.deanwild.materialshowcaseview.shape.OvalShape
import java.lang.Math.abs
import java.util.*

class SpotsListFragment : Fragment(), SpotCourseClickHandler {
	private val formatterYear = DateTimeFormatter.ofPattern("dd LLLL yyyy")
	private val formatterMonth = DateTimeFormatter.ofPattern("dd LLLL")
	private lateinit var mLogStatus: TextView
	private var mBinding: FragmentSpotsListBinding? = null
	private val binding get() = mBinding!!

	override fun onSaveInstanceState(outState: Bundle) {
	}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		mBinding = FragmentSpotsListBinding.inflate(inflater)
		return binding.root
	}

	override fun spotMenuClicked(spot: Spot, place: Place?, v: View) {
		val menu = PopupMenu(requireContext(), v)
		menu.setOnMenuItemClickListener { item ->
			when (item.itemId) {
				R.id.menu_edit ->
					findNavController().navigate(
						SpotsListFragmentDirections.actionSpotsListFragmentToEditSpotFragment(
							spot,
							place
						)
					)
				R.id.menu_add_course -> {
					val course = Course()
					course.coursespot = spot.id
					findNavController().navigate(
						SpotsListFragmentDirections.actionSpotsListFragmentToEditCourseFragment(
							course
						)
					)
				}
			}
			true
		}
		menu.inflate(R.menu.spot_menu)
		menu.show()
	}

	override fun courseEditClicked(course: Course) {
		findNavController().navigate(
			SpotsListFragmentDirections.actionSpotsListFragmentToEditCourseFragment(course)
		)
	}

	override fun courseRankingClicked(course: Course) {
		findNavController().navigate(
			SpotsListFragmentDirections.actionSpotsListFragmentToCourseRankingFrg(course)
		)
	}

	override fun courseClicked(course: Course) {
		findNavController().navigate(
			SpotsListFragmentDirections.actionSpotsListToShowCourseFragment(
				course.id,
				LocalDate.now()
			)
		)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		val lst = DBCtrl.get().getAllSpotsCourses()

		val curYear = Calendar.getInstance().get(Calendar.YEAR)
		val spotList = mutableListOf<SpotWithCoursesSumm>()
		for (spc in lst) {
			val ssi = SpotSummInfo(spc.spot!!, spc.place())
			val crs = mutableListOf<CourseSummInfo>()
			var lastSpotDate = LocalDateTime.MIN
			for (c in spc.courses!!) {
				var lastDate = LocalDateTime.MIN
				val sessions = DBCtrl.get().getCourseDates(c.id)
				var descCount = getString(R.string.courseNoRuns); var descDate = ""
				if (sessions.isNotEmpty()){
					descCount = getString(R.string.courseCountDesc, sessions.size.toString())
					lastDate = sessions[0]
					if (lastDate > lastSpotDate) lastSpotDate = lastDate
					val dtDesc = lastDate.format(if (lastDate.year == curYear) formatterMonth else formatterYear)
					descDate = getString(R.string.courseDateDesc, dtDesc)
				}
				crs.add(CourseSummInfo(c, descCount, descDate, lastDate))
			}
			crs.sortBy { it.lastDate }
			var pos = spotList.binarySearch { it.lastDate.compareTo(lastSpotDate) }
			if (pos < 0) pos = abs(pos) - 1
			spotList.add(pos, SpotWithCoursesSumm(ssi, crs, lastSpotDate))
		}
		spotList.reverse()

		with(binding.spotsView) {
			layoutManager = LinearLayoutManager(context)
			adapter = SpotsListRecyclerViewAdapter(spotList, this@SpotsListFragment)
		}
		registerForContextMenu(binding.spotsView)

		checkTips(spotList)

		binding.addSpotButton.setOnClickListener {addRenameSpotDialog(null)}
		binding.allHistoryButton.setOnClickListener {
			findNavController().navigate(
				SpotsListFragmentDirections.actionSpotsListFragmentToDaysHistoryFragment(
					0
				)
			)
		}
		binding.allNotesButton.setOnClickListener {
			findNavController().navigate(
				SpotsListFragmentDirections.actionSpotsListFragmentToDaysHistoryFragment(
					1
				)
			)
		}
		binding.settingsButton.setOnClickListener {
			findNavController().navigate(SpotsListFragmentDirections.actionSpotsListFragmentToSettingsFragment())
		}
		binding.mapButton.setOnClickListener {
			findNavController().navigate(
				SpotsListFragmentDirections.actionSpotsListFragmentToMapFragment(
					false
				)
			)
		}

		binding.unparsedButton.setOnClickListener {
			findNavController().navigate(SpotsListFragmentDirections.actionSpotsListFragmentToUnparsedListFragment())
		}
		binding.unparsedButton.visibility = if(DBCtrl.get().getUnparsedCount() > 0) VISIBLE else GONE;

		/* TODO
		mLogStatus = binding.logstatus
		val acc = GoogleSignIn.getLastSignedInAccount(requireContext())
		signLayout(acc)
		tryGoogleLogin()
		//logstatus visibility gone*/
	}

	private fun signLayout(acc: GoogleSignInAccount?){
		when(acc){
			null -> mLogStatus.text = getString(R.string.logOff)
			else -> mLogStatus.text = getString(R.string.loggedAs, acc.displayName)
		}
	}

	private fun tryGoogleLogin(){
		val prefs = requireContext().getSharedPreferences("login_prefs", Context.MODE_PRIVATE)
		if (prefs.contains(getString(R.string.login_startup_prefs))) return

		val builder = AlertDialog.Builder(requireContext())
		val dialogBinding = FragmentStartuploginBinding.inflate(LayoutInflater.from(context))
		builder.setView(dialogBinding.root)

		val alertDialog = builder.create()

		dialogBinding.signInButton.setOnClickListener {
			SignHelper.get().setListener { account-> signLayout(account) }
			SignHelper.get().singIn()
			alertDialog.cancel()
		}

		dialogBinding.laterButton.setOnClickListener {
			alertDialog.cancel()
		}

		val editor = prefs.edit()
		editor.putInt(getString(R.string.login_startup_prefs), -1)
		editor.apply()

		alertDialog.show()
	}

	private fun checkTips(lst: MutableList<SpotWithCoursesSumm>) {
		if (lst.count() == 0) {
			val spotTipShape = OvalShape(250)
			spotTipShape.isAdjustToTarget = false
			MaterialShowcaseView.Builder(requireActivity())
				.setTarget(binding.addSpotButton)
				.setDismissOnTouch(true)
				.setContentText(R.string.tip_add_spot)
				.singleUse(getString(R.string.tip_add_spot_id))
				.setDelay(100)
				.setShape(spotTipShape)
				.show()
		}
		else {
				Handler().postDelayed({
					//ListView doesn't populate at this moment somehow..
					val vw = (binding.spotsView.getChildAt(0) as ConstraintLayout).getChildAt(4)
					if (vw != null) {
						MaterialShowcaseView.Builder(requireActivity())
							.setTarget(vw)
							.setDismissOnTouch(true)
							.setContentText(getString(R.string.tip_add_course))
							.singleUse(getString(R.string.tip_add_course_id))
							.withRectangleShape()
							.show()
					}
				}, 100)
		}
	}

	private fun addRenameSpotDialog(spot: Spot?) {
		val place: Place? =
			if (spot != null) DBCtrl.get().getPlaceForSpot(spot.id) else null

		findNavController().navigate(
			SpotsListFragmentDirections.actionSpotsListFragmentToEditSpotFragment(spot, place)
		)
	}
}


package com.bess.skatenerd

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.databinding.ViewholderSpotBinding
import com.bess.skatenerd.databinding.ViewholderSpotCourseBinding
import com.bess.skatenerd.dbModel.Course
import com.bess.skatenerd.dbModel.Place
import com.bess.skatenerd.dbModel.Spot

interface SpotCourseClickHandler {
	fun courseEditClicked(course: Course)
	fun courseRankingClicked(course: Course)
	fun courseClicked(course: Course)
	fun spotMenuClicked(spot: Spot, place: Place?, v: View)
}

class SpotsListRecyclerViewAdapter(
	private val mFullValues: List<SpotWithCoursesSumm>,
	private val mEventHandler: SpotCourseClickHandler
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
	private var mValues: MutableList<SpotCourseItem> = mutableListOf()
	private val TYPE_SPOT = 0
	private val TYPE_COURSE = 1

	init{
		for ((cnt, dv) in mFullValues.withIndex()) {
			mValues.add(SpotCourseItem(dv.spot, null))
		}
		if(mFullValues.count() > 0){
			for (c in mFullValues[0].courses) {
				mValues.add(1, SpotCourseItem(null, c))
			}
			mValues[0].expanded = true
		}
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
		return if(viewType == TYPE_SPOT){
			val binding = ViewholderSpotBinding.inflate(LayoutInflater.from(parent.context), parent, false)
			ViewHolderSpot(binding)
		} else {
			val binding = ViewholderSpotCourseBinding.inflate(LayoutInflater.from(parent.context), parent, false)
			ViewHolderCourse(binding)
		}
	}

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
		val item = mValues[position]
		when(getItemViewType(position)){
			TYPE_SPOT ->
				(holder as ViewHolderSpot).initialize(item.spot!!.spot, item.spot.place, item.expanded)
			TYPE_COURSE ->
				(holder as ViewHolderCourse).initialize(item.course!!.course, item.course.countDesc, item.course.dateDesc)
		}
	}

	override fun getItemCount(): Int = mValues.size

	override fun getItemViewType(position: Int): Int {
		return if(mValues[position].spot != null) TYPE_SPOT else TYPE_COURSE
	}

	inner class ViewHolderSpot(private val binding: ViewholderSpotBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
		fun initialize(spot: Spot, place: Place?, initAnimate: Boolean){
			binding.gridtext.text = spot.name
			binding.address.visibility = if(place == null) GONE else VISIBLE
			if(place != null && place.desc.length > 40){
				val truncated = place.desc.substring(0, 40) + "..."
				binding.address.text = truncated
			}

			binding.root.setOnClickListener(this)
			binding.edit.setOnClickListener {mEventHandler.spotMenuClicked(spot, place, binding.edit)}
			binding.expand.setOnClickListener {onClick(it)}

			if(initAnimate) binding.expand.animate().rotation(90.0f).start();
		}

		override fun onClick(v: View) {
			val item = mValues[adapterPosition]
			val spotWithCourses = mFullValues.first { it.spot.spot.id == item.spot!!.spot.id }
			val count = spotWithCourses.courses.size
			if (item.expanded) {
				mValues.subList(adapterPosition + 1, adapterPosition + 1 + count).clear()
				notifyItemRangeRemoved(adapterPosition + 1, count)
			}
			else {
				for (c in spotWithCourses.courses)
					mValues.add(adapterPosition + 1, SpotCourseItem(null, c))
				notifyItemRangeInserted(adapterPosition + 1, count)
			}
			binding.expand.animate().rotation(if (item.expanded) 0.0f else 90.0f).setDuration(300).start();
			item.expanded = !item.expanded
		}
	}

	inner class ViewHolderCourse(private val binding: ViewholderSpotCourseBinding) : RecyclerView.ViewHolder(binding.root){
		fun initialize(course: Course, countDesc: String, dateDesc: String){
			binding.gridtext.text = course.name
			binding.descCount.text = countDesc
			binding.descDate.text = dateDesc

			binding.root.setOnClickListener{mEventHandler.courseClicked(course)}
			binding.edit.setOnClickListener {mEventHandler.courseEditClicked(course)}
			if(course.serverId != null) {
				binding.ranking.visibility = VISIBLE
				binding.ranking.setOnClickListener { mEventHandler.courseRankingClicked(course) }
			}
			else{
				binding.ranking.visibility = GONE
				binding.ranking.setOnClickListener(null)
			}
		}
	}

	data class SpotCourseItem(val spot: SpotSummInfo?, val course: CourseSummInfo?, var expanded: Boolean = false)
}

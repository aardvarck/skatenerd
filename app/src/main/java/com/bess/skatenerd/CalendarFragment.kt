package com.bess.skatenerd


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.applandeo.materialcalendarview.CalendarView
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId

class CalendarFragment : Fragment() {
	private var mCourseId: Int = 0
	private lateinit var calendarView: CalendarView

	override fun onCreateView(
			inflater: LayoutInflater, container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		val args = CalendarFragmentArgs.fromBundle(requireArguments())
		mCourseId = args.courseId

		val res = inflater.inflate(R.layout.fragment_calendar, container, false)

		calendarView = res.findViewById(R.id.calendarView)
		calendarView.setOnDayClickListener { eventDay ->
			val clickedDayCalendar = eventDay.calendar
			val date =
					Instant.ofEpochMilli(clickedDayCalendar.time!!.time).atZone(ZoneId.systemDefault())
							.toLocalDate()
			res.findNavController()
					.navigate(
							CalendarFragmentDirections.actionCalendarFragmentToShowCourseFragment(
									mCourseId,
									date
							)
					)
		}

		return res
	}
}

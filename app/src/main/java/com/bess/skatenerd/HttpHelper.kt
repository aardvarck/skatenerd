package com.bess.skatenerd

import android.content.Context
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bess.skatenerd.dbModel.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.json.JSONObject
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime

interface OnPublishedListener{
	fun published(serverId: Int)
}

// https://stackoverflow.com/questions/40398072/singleton-with-parameter-in-kotlin
class HttpHelper(private val mContext: Context, private val mAct: MainActivity) {
	companion object {
		@Volatile private var INSTANCE: HttpHelper? = null
		fun createInstance(context: Context, act: MainActivity): HttpHelper =
			INSTANCE ?: synchronized(this) {INSTANCE ?: buildHelper(context, act).also { INSTANCE = it }}
		private fun buildHelper(context: Context, act: MainActivity) = HttpHelper(context, act)
		fun get(): HttpHelper{return INSTANCE!!}
	}

	fun publishSpot(spot: Spot, place: Place, successListener: (Int) -> Unit) {
		val queue = Volley.newRequestQueue(mContext)
		val url = "http://192.168.0.11:8000/spot"

		val jsonObject = JSONObject()
		jsonObject.put("name", spot.name)
		jsonObject.put("descr", spot.desc)
		jsonObject.put("lat", place.lat)
		jsonObject.put("lon", place.lon)
		jsonObject.put("spotuser", SignHelper.get().mUserSrvId)

		//val body = jsonObject.toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())

		val jsonRequest = JsonObjectRequest(com.android.volley.Request.Method.POST, url, jsonObject,
				{ response ->
					if (response.length() > 0) {
						successListener(response.getInt("id"))
					} else {
						//view.pubSpotDesc.text = "there are some spots nearby"
					}
				},
				{ /*textView.text = "That didn't work!"*/ })
		queue.add(jsonRequest)
	}


	fun publishCourse(srvSpotId: Int, course: CourseRawInfo, successListener: (Int) -> Unit) {
		val queue = Volley.newRequestQueue(mContext)
		val url = "http://192.168.0.11:8000/course"

		val jsonObject = JSONObject()
		jsonObject.put("name", course.name)
		jsonObject.put("descr", course.desc)
		jsonObject.put("penalty", course.penalty)
		jsonObject.put("coursespot", srvSpotId)
		jsonObject.put("courseuser", SignHelper.get().mUserSrvId)

		val jsonRequest = JsonObjectRequest(com.android.volley.Request.Method.POST, url, jsonObject,
			{ response ->
				if (response.length() > 0) {
					successListener(response.getInt("id"))
				} else {
					//view.pubSpotDesc.text = "there are some spots nearby"
				}
			},
			{ /*textView.text = "That didn't work!"*/ })

		queue.add(jsonRequest)
	}

	fun publishRuns(srvCourseId: Int, runs: List<Run>, successListener: (Boolean) -> Unit) {
		val queue = Volley.newRequestQueue(mContext)
		val url = "http://192.168.0.11:8000/pubruns"
		val jsonObject = JSONObject()
		jsonObject.put("timecourse", srvCourseId)
		jsonObject.put("runuser", SignHelper.get().mUserSrvId)

		var xx = 0
		for(i in runs){
			xx += 1
			if (xx > 10) break
			val runJson = JSONObject()
			runJson.put("s", i.s)
			runJson.put("ms", i.ms)
			runJson.put("cones", i.cones)
			runJson.put("created", i.created)
			jsonObject.accumulate("runs", runJson)
		}

		val jsonRequest = JsonObjectRequest( com.android.volley.Request.Method.POST, url, jsonObject,
			{ response ->
				if(response.length() == 0){
					//view.pubSpotDesc.text = getString(R.string.pubSpotDescNoSiblings)
				}
				else{
					//view.pubSpotDesc.text = "there are some spots nearby"
				}
			},
			{ /*textView.text = "That didn't work!"*/ })

		queue.add(jsonRequest)
	}

	fun unpublishCourse(courseSrvId: Int, successListener: (Boolean) -> Unit) {
		val queue = Volley.newRequestQueue(mContext)
		val url = "http://192.168.0.11:8000/uncourse"

		val jsonObject = JSONObject()
		jsonObject.put("id", courseSrvId)
		jsonObject.put("courseuser", SignHelper.get().mUserSrvId)

		val jsonRequest = JsonObjectRequest(com.android.volley.Request.Method.POST, url, jsonObject,
				{ response ->
					if (response.length() > 0) {
						successListener(true)
					} else {
						successListener(false)
					}
				},
				{
					/*textView.text = "That didn't work!"*/
				})

		queue.add(jsonRequest)
	}

	/*public fun getNearbySpots(lat: Double, lon: Double): List<PlaceInfo> {
		GlobalScope.launch(Dispatchers.IO) {
			val client = OkHttpClient()
			val jsonObject = JSONObject()
			jsonObject.put("lat", lat)
			jsonObject.put("lon", lon)
			//val body = jsonObject.toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())
            val urlTmp = "http://192.168.0.11:8000/nearby/%f/%f"
			val request = Request.Builder().url(urlTmp.format(lat, lon)).get().build();
			val response = client.newCall(request).execute();
			val places = mutableListOf<PlaceInfo>()
			if (response.isSuccessful) {
				val ss = response.body!!.string()
				Toast.makeText(mContext, ss, Toast.LENGTH_SHORT).show()
			} else {
			}
			return places.toList()
		}
	}*/

	data class CourseRunsWeb(val runs: MutableList<Run> = mutableListOf(), val users: MutableMap<Int, User> = mutableMapOf())

	fun getCourseRuns(courseSrvId: Int, successListener: (CourseRunsWeb) -> Unit){
		val queue = Volley.newRequestQueue(mContext)
		val url = "http://192.168.0.11:8000/getruns"

		val jsonObject = JSONObject()
		jsonObject.put("id", courseSrvId)

		val jsonRequest = JsonObjectRequest(com.android.volley.Request.Method.POST, url, jsonObject,
				{ response ->
					if (response.length() > 0) {
						val crw = CourseRunsWeb()
						val users = response.getJSONArray("users")
						for(i in 0 until users.length()){
							val u = users.getJSONObject(i)
							val uw = User(serverId = u.getInt("id"), fname = u.getString("fnm"), gname = u.getString("gnm"))
							crw.users[uw.serverId!!] = uw
						}
						val runs = response.getJSONArray("runs")
						for(i in 0 until runs.length()){
							val r = runs.getJSONObject(i)
							val rw = Run( 0, s = r.getInt("s"), ms = r.getInt("ms"),
											cones = r.getInt("cones"), userid = r.getInt("usr"), created = LocalDateTime.parse(r.getString("cr")), serverId = 0, timecourse = 0)
							crw.runs.add(rw)
						}

						successListener(crw)
					} else {
					}
				},
				{ /*textView.text = "That didn't work!"*/ })

		queue.add(jsonRequest)
	}

	fun signIn(user: User, token: String, successListener: (Int) -> Unit) {
		val queue = Volley.newRequestQueue(mContext)
		val url = "http://192.168.0.11:8000/signin"

		val jsonObject = JSONObject()
		jsonObject.put("id", 0)
		jsonObject.put("name", user.name)
		jsonObject.put("email", user.email)
		jsonObject.put("token", token)

		val jsonRequest = JsonObjectRequest(com.android.volley.Request.Method.POST, url, jsonObject,
			{ response ->
				if (response.length() > 0) {
					successListener(response.getInt("id"))
				} else {
				}
			},
			{ /*textView.text = "That didn't work!"*/ })

		queue.add(jsonRequest)
	}
}



package com.bess.skatenerd

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout

class TripleDigit(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

	private val mHighPicker: DigitPicker
	private val mMidPicker: DigitPicker
	private val mLowPicker: DigitPicker
	private val mShowDigitButton: Button

	var value: Int
		get() = mHighPicker.value * 100 + mMidPicker.value * 10 + mLowPicker.value
		set(`val`) {
			mHighPicker.value = `val` / 100
			mMidPicker.value = (`val` % 100) / 10
			mLowPicker.value = `val` % 10
		}

	init {
		orientation = HORIZONTAL
		gravity = Gravity.CENTER_HORIZONTAL

		val inflater = context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
		inflater.inflate(R.layout.triple_digit, this, true)

		mHighPicker = findViewById(R.id.digit_high)
		mMidPicker = findViewById(R.id.digit_mid)
		mLowPicker = findViewById(R.id.digit_low)

		mShowDigitButton = findViewById(R.id.showDigitButton)
		mShowDigitButton.setOnClickListener {
			mShowDigitButton.visibility = View.GONE
			mLowPicker.visibility = View.VISIBLE
		}
	}

	fun init(showMicroDigit: Boolean) {
		mHighPicker.init(9, 0, 1, true, leadingZeroes = false)
		mMidPicker.init(9, 0, 1, true, leadingZeroes = false)
		mLowPicker.init(9, 0, 1, true, leadingZeroes = false)

		mLowPicker.visibility = if (showMicroDigit) View.VISIBLE else View.GONE
		mShowDigitButton.visibility = if (showMicroDigit) View.GONE else View.VISIBLE
	}
}
package com.bess.skatenerd

import android.content.Context
import android.text.InputFilter
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView

class DigitPicker(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

	private lateinit var vl: TextView
	private val bPlus: Button
	private val bMinus: Button
	private var max: Int = 0
	private var min: Int = 0
	private var mDigits: Int = 0
	private var loop: Boolean = false
	private var mLeadZero: Boolean = false

	var value: Int
		get() = Integer.valueOf(vl.text.toString())
		set(`val`) {
			if (mLeadZero)
				vl.text = String.format("%02d", `val`)
			else
				vl.setText(Integer.toString(`val`))
			if (!loop) {
				bMinus.isEnabled = (`val` != min)
				bPlus.isEnabled = (`val` != max)
			}
		}

	private var plusHandler = OnClickListener {
		var cur = Integer.valueOf(vl.text.toString())
		if (cur < max)
			cur++
		else if (cur == max && loop)
			cur = min
		value = cur
	}
	private var minusHandler = OnClickListener {
		var cur = Integer.valueOf(vl.text.toString())
		if (cur > min)
			cur--
		else if (cur == min && loop)
			cur = max
		value = cur
	}

	init {
		orientation = VERTICAL
		gravity = Gravity.CENTER_HORIZONTAL

		val inflater = context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
		inflater.inflate(R.layout.digit_picker, this, true)

		vl = findViewById(R.id.num)
		bPlus = findViewById(R.id.plus)
		bPlus.setOnClickListener(plusHandler)
		bMinus = findViewById(R.id.minus)
		bMinus.setOnClickListener(minusHandler)
	}

	fun init(max_value: Int, min_value: Int, digits: Int, isLoop: Boolean, leadingZeroes: Boolean) {
		max = max_value
		min = min_value
		loop = isLoop
		mLeadZero = leadingZeroes
		mDigits = digits
		vl.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(digits))
	}
}
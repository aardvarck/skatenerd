package com.bess.skatenerd

import android.Manifest
import android.app.Dialog

import android.content.pm.PackageManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.RunTime
import com.bess.skatenerd.dbModel.Unparsed
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle
import java.io.Serializable

class UnparsedAdapter(
		private var lst: MutableList<Unparsed>
) {
	var index: Int = 0
		private set
	val count: Int
		get() = lst.count()
	val current: Unparsed?
		get() = if (index < lst.count()) lst[index] else null
	val nextAvailable: Boolean
		get() = (index + 1) < lst.count()
	val prevAvailable: Boolean
		get() = index > 0

	fun goNext() {
		if (nextAvailable) index++
	}

	fun goPrev() {
		if (prevAvailable) index--
	}

	fun removeCurrent() {
		if (lst.count() > index) {
			lst.removeAt(index)
			index = 0
		}
	}
}

class AddTimeFragment : DialogFragment() {
	private var mTimeId: Int = -1

	private lateinit var mSecPicker: DigitPicker
	private lateinit var mConesPicker: DigitPicker
	private lateinit var mMsecPicker: TripleDigit
	private lateinit var mParseTxtView: TextView
	private lateinit var mParseResTxtView: TextView
	private lateinit var mPBar: ProgressBar
	private var mUseMic: Boolean = false
	private var mCourseId: Int = 0
	private var mDate: LocalDate = LocalDate.now()
	private lateinit var mUnparsedAdapter: UnparsedAdapter
	private var mUnparsed: Boolean = false
	private var mShowMicroPart: Boolean = false
	private lateinit var mPrevButton: ImageButton
	private lateinit var mNextButton: ImageButton
	private lateinit var mUnparsedRunsOfTxtView: TextView
	private lateinit var mDateUnparsedTxtView: TextView
	private lateinit var mAddButton: Button
	private lateinit var mDiscardButton: Button
	private lateinit var mMicButton: Button
	private lateinit var mLabelEditTxt: EditText

	private lateinit var mSpeechRecog: RecogListenerHelper

	private val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)

	val time: Double
		get() = mSecPicker.value + mMsecPicker.value * 0.01

	val cones: Int
		get() = mConesPicker.value

	private val REQUEST_RECORD_PERMISSION = 100
	override fun onRequestPermissionsResult(
			requestCode: Int,
			permissions: Array<String>,
			grantResults: IntArray
	) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		when (requestCode) {
			REQUEST_RECORD_PERMISSION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				mParseTxtView.visibility = VISIBLE
				mParseResTxtView.visibility = VISIBLE
				mPBar.visibility = VISIBLE

				mSpeechRecog.startRecognize()
			}
		}
	}

	private fun tryToParse(txt: String) {
		val recogRes = TimeRecogHelper.tryToParse(txt)
		when (recogRes.type) {
			RecogResType.Partial -> {
				mParseResTxtView.setText(R.string.time_parse_part_failed)
			}
			RecogResType.Partial, RecogResType.Success -> {
				mSecPicker.value = recogRes.time!!.s
				mMsecPicker.value = recogRes.time.ms
				mConesPicker.value = recogRes.time.cones
				mLabelEditTxt.setText(recogRes.time.note)
			}
			RecogResType.Failed -> {
				mParseResTxtView.setText(R.string.time_parse_failed)
			}
		}
	}

	private fun setUnparsedLayout() {
		if (mUnparsed) {
			mUnparsedAdapter =
					UnparsedAdapter(DBCtrl.get().getUnparsedForCourse(mCourseId, mDate).toMutableList())
			updateUnparsedLayout()
		}

		val parsePartVisibility = if (mUnparsed) VISIBLE else GONE
		mPrevButton.visibility = parsePartVisibility
		mNextButton.visibility = parsePartVisibility
		mUnparsedRunsOfTxtView.visibility = parsePartVisibility
		mDateUnparsedTxtView.visibility = parsePartVisibility
		mParseTxtView.visibility = parsePartVisibility
		mDiscardButton.visibility = parsePartVisibility

		//TODO
		//mMicButton.visibility = if (mUnparsed) GONE else VISIBLE
		mMicButton.visibility = GONE
	}

	private fun updateUnparsedLayout() {
		mPrevButton.isEnabled = mUnparsedAdapter.prevAvailable
		mNextButton.isEnabled = mUnparsedAdapter.nextAvailable

		mUnparsedRunsOfTxtView.text = requireContext().getString(
				R.string.unparsed_run_of_runs,
				mUnparsedAdapter.index + 1, mUnparsedAdapter.count
		)

		mSecPicker.value = 0
		mMsecPicker.value = 0
		mConesPicker.value = 0
		mLabelEditTxt.text.clear()

		val curUnparsed = mUnparsedAdapter.current
		if (curUnparsed != null) {
			mParseTxtView.setText(curUnparsed.data)
			mDateUnparsedTxtView.setText(formatter.format(curUnparsed.created))

			tryToParse(curUnparsed.data)
		} else {
			findNavController().navigate(
					AddTimeFragmentDirections.actionAddTimeFragmentToShowCourseFragment(
							mCourseId,
							mDate
					)
			)
		}
	}

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		outState.putInt("course_id", mCourseId);
		outState.putSerializable("date", mDate as Serializable)
		outState.putBoolean("dounparsed", mUnparsed);
		outState.putBoolean("mic", mUseMic)
		outState.putInt("tm_s", mSecPicker.value)
		outState.putInt("tm_ms", mMsecPicker.value)
		outState.putInt("cones", mConesPicker.value)
		outState.putInt("time_id", mTimeId)
		outState.putBoolean("show_micro_digit", mShowMicroPart)
	}

	private fun applyStateArgs(args: AddTimeFragmentArgs) {
		mSecPicker.init(99, 0, 2, true, leadingZeroes = true)
		mSecPicker.value = args.tmS

		mMsecPicker.init(mShowMicroPart)
		mMsecPicker.value = args.tmMs

		mConesPicker.init(99, 0, 2, false, leadingZeroes = false)
		mConesPicker.value = args.cones

		mTimeId = args.timeId
		if (mTimeId != -1) {
			val ri = DBCtrl.get().getRun(mTimeId)
			if (ri != null) {
				mSecPicker.value = ri.run.s
				mMsecPicker.value = ri.run.ms
				mConesPicker.value = ri.run.cones
				mLabelEditTxt.setText(ri.label()?.data)
			}
		}
	}

	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		val args = AddTimeFragmentArgs.fromBundle(savedInstanceState ?: requireArguments())
		mCourseId = args.courseId
		mDate = args.date
		mUseMic = args.mic
		mUnparsed = args.dounparsed
		mTimeId = args.timeId
		mShowMicroPart = args.showMicroDigit

		if (mUseMic) {
			requestPermissions(arrayOf(Manifest.permission.RECORD_AUDIO), REQUEST_RECORD_PERMISSION)
		}

		mSpeechRecog = RecogListenerHelper(requireContext(), object : RecogListenCallbacker {
			override fun onResult(txt: String) {
				mParseTxtView.setText(txt)
				tryToParse(txt)
			}

			override fun onEndOfSpeech() {
				mPBar.visibility = GONE
			}
		})

		val builder =
				AlertDialog.Builder(requireActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar)
		val dialogView = LayoutInflater.from(context).inflate(R.layout.fragment_add_time, null)
		builder.setView(dialogView)

		mSecPicker = dialogView.findViewById(R.id.digitpicker_s)
		mMsecPicker = dialogView.findViewById(R.id.pairdigit_ms)
		mConesPicker = dialogView.findViewById(R.id.digitpicker_con)

		mDiscardButton = dialogView.findViewById(R.id.discardButton)
		mDiscardButton.setOnClickListener {
			val unp = mUnparsedAdapter.current
			if (unp != null) {
				DBCtrl.get().removeUnparsed(unp)
				mUnparsedAdapter.removeCurrent()
			}
			updateUnparsedLayout()
		}

		mAddButton = dialogView.findViewById(R.id.addButton)
		mAddButton.setText(if (mUnparsed || mTimeId != -1) R.string.confirm else R.string.add)
		mAddButton.setOnClickListener {
			if (!mUnparsed) {
				if (mTimeId == -1) {
					val newDt = getNewDateTime()
					DBCtrl.get().addTime(
							getTime(),
							mCourseId,
							mConesPicker.value,
							newDt,
							mLabelEditTxt.text.trim().toString()
					)
				} else {
					DBCtrl.get().editTime(
							mTimeId,
							getTime(),
							mCourseId,
							mConesPicker.value,
							mLabelEditTxt.text.trim().toString()
					)
				}
				findNavController().navigate(
						AddTimeFragmentDirections.actionAddTimeFragmentToShowCourseFragment(
								mCourseId,
								mDate
						)
				)
			} else {
				val unp = mUnparsedAdapter.current
				if (unp != null) {
					DBCtrl.get().removeUnparsed(unp)
				 	mUnparsedAdapter.removeCurrent()
					DBCtrl.get().addTime(
							getTime(),
							mCourseId,
							mConesPicker.value,
							unp.created,
							mLabelEditTxt.text.trim().toString()
					)
				}
				updateUnparsedLayout()
			}
		}

		mMicButton = dialogView.findViewById(R.id.micButton)
		mMicButton.setOnClickListener {
			requestPermissions(arrayOf(Manifest.permission.RECORD_AUDIO), REQUEST_RECORD_PERMISSION)
		}

		mParseTxtView = dialogView.findViewById(R.id.parseTxtView)
		mParseResTxtView = dialogView.findViewById(R.id.parseResTxtView)
		mPBar = dialogView.findViewById(R.id.pBar)
		if (!mUseMic) {
			mParseTxtView.visibility = GONE
			mParseResTxtView.visibility = GONE
			mPBar.visibility = GONE
		}

		mPrevButton = dialogView.findViewById(R.id.prevButton)
		mPrevButton.setOnClickListener {
			mUnparsedAdapter.goPrev()
			updateUnparsedLayout()
		}
		mNextButton = dialogView.findViewById(R.id.nextButton)
		mNextButton.setOnClickListener {
			mUnparsedAdapter.goNext()
			updateUnparsedLayout()
		}
		mUnparsedRunsOfTxtView = dialogView.findViewById(R.id.unparsedOfRunsTxtView)
		mDateUnparsedTxtView = dialogView.findViewById(R.id.dateUnparsedTxtView)
		mLabelEditTxt = dialogView.findViewById(R.id.labelEditTxt)

		applyStateArgs(args)

		setUnparsedLayout()

		return builder.create()
	}

	private fun getNewDateTime(): LocalDateTime {
		return when (mDate) {
			LocalDate.now() -> LocalDateTime.now()
			else -> {
				val courseRuns = DBCtrl.get().getCourseRunsForDate(mCourseId, mDate)
				when (courseRuns.isEmpty()) {
					true -> mDate.atTime(12, 0)
					false -> courseRuns.last().run.created.plusMinutes(3)
				}
			}
		}
	}

	private fun getTime(): RunTime {
		return RunTime(mSecPicker.value, mMsecPicker.value)
	}
}

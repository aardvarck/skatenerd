package com.bess.skatenerd

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences.Editor
import android.net.Uri
import android.os.Bundle
import android.speech.SpeechRecognizer
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.RunTime
import com.bess.skatenerd.dbModel.SpotWithCourses
import com.google.android.gms.actions.NoteIntents
import com.jakewharton.threetenabp.AndroidThreeTen
import org.threeten.bp.LocalDateTime
import java.io.OutputStream


const val RC_SIGN_IN = 123
const val RC_SAVE_DB = 124
const val RC_LOAD_DB = 125

class MainActivity : AppCompatActivity()//,RecognitionListener
{
	private lateinit var recognizerIntent: Intent
	private lateinit var speech: SpeechRecognizer
	private val REQUEST_RECORD_PERMISSION = 100

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		AndroidThreeTen.init(this)

		supportActionBar?.hide()

		setContentView(R.layout.activity_main)

		DBCtrl.createInstance(this)

		checkForStartupTips()

		handleIntent(intent)

		HttpHelper.createInstance(this, this)
		SignHelper.createInstance(this, this, DBCtrl.get()).trySilentSignIn()
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)

		if (requestCode == RC_SIGN_IN) {
			//getFragment(SettingsFragment::class.java)?.updateSignStatus()

			SignHelper.get().updateSignStatus()
		}

		if ((requestCode == RC_SAVE_DB || requestCode == RC_LOAD_DB) && resultCode == RESULT_OK) {
			try {
				val uri: Uri = data?.data!!
				if (requestCode == RC_SAVE_DB) {
					val output: OutputStream = contentResolver.openOutputStream(uri)!!
					getDatabasePath("skatedb").inputStream().copyTo(output)
				} else {
					DBCtrl.get().closeDb()
					val input = contentResolver.openInputStream(uri)!!
					input.copyTo(getDatabasePath("skatedb").outputStream())
					DBCtrl.get().upgradeDb(this)
				}
				val ts =
						if (requestCode == RC_SAVE_DB) R.string.pref_export_success else R.string.pref_import_success
				Toast.makeText(this, ts, Toast.LENGTH_SHORT).show()
			} catch (e: Throwable) {
				Toast.makeText(this, getString(R.string.pref_error), Toast.LENGTH_SHORT).show()
			}
		}
	}

	private fun checkForStartupTips() {
		var editor: Editor? = null
		var spotCourses: List<SpotWithCourses>? = null
		val prefs = getSharedPreferences("material_showcaseview_prefs", Context.MODE_PRIVATE)
		if (!prefs.contains(getString(R.string.tip_prefix) + getString(R.string.tip_add_spot_id))) {
			spotCourses = DBCtrl.get().getAllSpotsCourses()
			if (spotCourses.count() > 0) {
				editor = prefs.edit()
				editor.putInt(
					getString(R.string.tip_prefix) + getString(R.string.tip_add_spot_id),
					-1
				)
			}
		}
		if (!prefs.contains(getString(R.string.tip_prefix) + getString(R.string.tip_add_course_id))) {
			if (spotCourses == null) spotCourses = DBCtrl.get().getAllSpotsCourses()
			val hasCourses = spotCourses.any { val cs = it.courses; cs != null && cs.count() > 0 }
			if (hasCourses) {
				if (editor == null) editor = prefs.edit()
				editor?.putInt(
					getString(R.string.tip_prefix) + getString(R.string.tip_add_course_id),
					-1
				)
			}
		}
		if (!prefs.contains(getString(R.string.tip_prefix) + getString(R.string.tip_show_course_id))) {
			if (DBCtrl.get().getLastRun() != null) {
				if (editor == null) editor = prefs.edit()
				editor?.putInt(
					getString(R.string.tip_prefix) + getString(R.string.tip_show_course_id),
					-1
				)
			}
		}
		if (editor != null) editor.apply()
	}


	private fun handleIntent(intent: Intent?) {
		when {
			intent?.action == NoteIntents.ACTION_CREATE_NOTE -> {
				if ("text/plain" == intent.type) {
					val noteStr = intent.extras?.get(Intent.EXTRA_TEXT)?.toString() ?: return
					val showCourseFrag = getFragment(ShowCourseFragment::class.java)
					if (showCourseFrag != null) {
						val courseId = showCourseFrag.getCourseId()
						val recogRes = TimeRecogHelper.tryToParse(noteStr)

						when (recogRes.type) {
							RecogResType.Success -> {
								val rTime = recogRes.time!!
								DBCtrl.get().addTime(
									RunTime(rTime.s, rTime.ms),
									courseId,
									rTime.cones,
									LocalDateTime.now(),
									rTime.note ?: ""
								)
							}
							else -> {
								DBCtrl.get().addUnparsed(courseId, noteStr)
							}
						}
						showCourseFrag.updateData()
					} else {
						DBCtrl.get().addUnparsed(null, noteStr)
					}

				}
			}
		}
	}

	override fun onNewIntent(intent: Intent?) {
		super.onNewIntent(intent)
		handleIntent(intent)
	}

	override fun onRequestPermissionsResult(
		requestCode: Int,
		permissions: Array<String>,
		grantResults: IntArray
	) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
	}

	@Suppress("UNCHECKED_CAST")
	fun <F : Fragment> getFragment(fragmentClass: Class<F>): F? {
		val navHostFragment = this.supportFragmentManager.fragments.first() as NavHostFragment
		navHostFragment.childFragmentManager.fragments.forEach {
			if (fragmentClass.isAssignableFrom(it.javaClass)) {
				return it as F
			}
		}
		return null
	}


}

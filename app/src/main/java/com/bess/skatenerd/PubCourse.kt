package com.bess.skatenerd

import android.graphics.Point
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.bess.skatenerd.databinding.FragmentPubcourseBinding
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.Course
import com.bess.skatenerd.dbModel.CourseRawInfo
import com.bess.skatenerd.dbModel.Place
import com.bess.skatenerd.dbModel.Spot

class PubCourse : DialogFragment() {
	private lateinit var mSpot: Spot
	private lateinit var mPlace: Place
	private lateinit var mCourse: Course
	private var mBinding: FragmentPubcourseBinding? = null
	private val binding get() = mBinding!!

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val args = savedInstanceState ?: requireArguments()
		if (args != null) {
			mSpot = args.getParcelable("spot")!!
			mCourse = args.getParcelable("course")!!
			mPlace = args.getParcelable("place")!!
		}
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		dialog?.window?.attributes?.windowAnimations = R.style.PubDialogAnimations
	}

	override fun onResume() {
		super.onResume()
		val window: Window = dialog!!.window!!
		val display: Display = window.windowManager.defaultDisplay
		val size = Point()
		display.getSize(size)
		window.setLayout((size.x * 0.95).toInt(), (size.y * 0.95).toInt())
		window.setGravity(Gravity.CENTER)
	}

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)

	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		mBinding = FragmentPubcourseBinding.inflate(inflater)
		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding.courseName.editText!!.setText(mCourse.name)
		binding.courseDesc.editText!!.setText(mCourse.desc)

		binding.publishButton.setOnClickListener {
			HttpHelper.get().publishSpot(mSpot, mPlace){ spotSrvId ->
				DBCtrl.get().setSpotSrvId(mSpot.id, spotSrvId)
				HttpHelper.get().publishCourse(spotSrvId, CourseRawInfo.fromCourse(mCourse)){ courseSrvId ->
					DBCtrl.get().setCourseSrvId(mCourse.id, courseSrvId)
					mCourse.serverId = courseSrvId
					val cr = DBCtrl.get().getCourseRuns(mCourse.id)
					HttpHelper.get().publishRuns(mCourse.serverId!!, cr){

					}
				}
			}
		}
		binding.backButton.setOnClickListener {findNavController().navigateUp()}
	}
}
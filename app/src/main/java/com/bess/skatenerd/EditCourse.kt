package com.bess.skatenerd

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.bess.skatenerd.databinding.FragmentNewCourseBinding
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.Course
import com.google.android.material.textfield.TextInputLayout


class EditCourse : Fragment() {
	private lateinit var mNameView: TextInputLayout
	private lateinit var mDescView: TextInputLayout
	private lateinit var mPenaltyView: Spinner
	private lateinit var mPublishBtn: Button
	private lateinit var mUnpublishBtn: Button
	private lateinit var mExportBtn: Button
	private lateinit var mPublishedDesc: TextView
	private var mCurCourse = Course()
	private var mCourseChanged = false
	private var mBinding: FragmentNewCourseBinding? = null
	private val binding get() = mBinding!!

	private fun saveData(){
		mCurCourse.name = mNameView.editText!!.text.toString()
		mCurCourse.desc = mDescView.editText!!.text.toString()
		mCurCourse.penalty = mPenaltyView.selectedItemPosition
	}

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		saveData()
		outState.putParcelable("course", mCurCourse)
		outState.putBoolean("courseChanged", mCourseChanged)
	}

	override fun onResume() {
		super.onResume()
		mNameView.post {
			mNameView.requestFocus()
			val imm = mNameView.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
			imm.showSoftInput(mNameView, InputMethodManager.SHOW_IMPLICIT)
		}
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		mBinding = FragmentNewCourseBinding.inflate(inflater)
		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		val args = EditCourseArgs.fromBundle(savedInstanceState ?: requireArguments())
		mCurCourse = args.course
		if(savedInstanceState != null)
			mCourseChanged = savedInstanceState.getBoolean("courseChanged")

		mNameView = view.findViewById(R.id.nameEdit)
		mNameView.editText!!.setText(mCurCourse.name)

		mDescView = view.findViewById(R.id.descEdit)
		mDescView.editText!!.setText(mCurCourse.desc)

		mPenaltyView = view.findViewById(R.id.penaltySpin)
		mPenaltyView.setSelection(mCurCourse.penalty)


		binding.okButton.isEnabled = mCourseChanged
		binding.okButton.setOnClickListener {
			saveData()
			makeDecision(false)
			findNavController().navigateUp()
		}
		binding.cancelButton.setOnClickListener {findNavController().navigateUp()}

		mPublishBtn = binding.publishButton
		mPublishBtn.setOnClickListener {
			if(SignHelper.get().mPublishEnable){
				val spot = DBCtrl.get().getSpotInfo(mCurCourse.coursespot)
				val place = DBCtrl.get().getPlaceForSpot(spot.id)
				when(spot.serverId) {
					null -> {
						findNavController().navigate(EditCourseDirections.actionEditCourseToPubSpot(spot, mCurCourse, place))
					}
					else -> {
						findNavController().navigate(EditCourseDirections.actionEditCourseToPubCourse(spot, mCurCourse, place))
					}
				}
			}
			else{
				val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
				builder.setMessage(getString(R.string.needToLogIn))
						.setPositiveButton(getString(R.string.Ok)) { _, _ ->}.show()
			}
		}

		mUnpublishBtn = binding.unpublishButton
		mUnpublishBtn.setOnClickListener {
			if(mCurCourse.serverId != null){
				HttpHelper.get().unpublishCourse(mCurCourse.serverId!!){
					DBCtrl.get().unpublishCourse(mCurCourse.id)
					mCurCourse.serverId = null
					updatePublishState()
				}
			}
		}

		mPublishedDesc = binding.publishedText

		mExportBtn = binding.exportXlsButton
		mExportBtn.setOnClickListener {
			ExportXlsHelper.exportCourse(requireContext(), requireActivity(), mCurCourse, null)
		}

		binding.deleteButton.isEnabled = (mCurCourse.id != 0)
		binding.deleteButton.setOnClickListener {
			val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
			builder.setMessage(getString(R.string.deleteCourseWarning)).setNegativeButton(getString(R.string.Cancel), null)
				.setPositiveButton(getString(R.string.delete)) { _, _ ->
					makeDecision(true)
					findNavController().navigateUp()
				}.show()
		}

		val tw : TextWatcher = object: TextWatcher {
			override fun afterTextChanged(s: Editable) { binding.okButton.isEnabled = mNameView.editText!!.text.isNotEmpty(); mCourseChanged = true}
			override fun beforeTextChanged(s: CharSequence?, a: Int, b: Int, c: Int) {}
			override fun onTextChanged(s: CharSequence?, a: Int, b: Int, c: Int) {}
		}
		mNameView.editText!!.addTextChangedListener(tw)
		mDescView.editText!!.addTextChangedListener(tw)

		updatePublishState()
	}

	private fun updatePublishState(){
		val published = mCurCourse.serverId != null
		mPublishBtn.visibility = if(published) GONE else VISIBLE;
		mUnpublishBtn.visibility = if(published) VISIBLE else GONE;
		mPublishedDesc.visibility = if(published) VISIBLE else GONE;
	}


	private fun makeDecision(delete: Boolean){
		if(delete)
			DBCtrl.get().removeCourse(mCurCourse.id)
		else{
			if(mCurCourse.id == 0)
				DBCtrl.get().createNewCourse(mCurCourse)
			else{
				DBCtrl.get().setCourseDetails(mCurCourse)
			}
		}
	}
}
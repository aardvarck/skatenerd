package com.bess.skatenerd.dbModel

import android.os.Parcelable
import androidx.room.*
import kotlinx.parcelize.Parcelize
import org.threeten.bp.LocalDateTime

@Parcelize
@Entity(
		tableName = "course",
		foreignKeys = [ForeignKey(
				entity = Spot::class, parentColumns = arrayOf("id"), childColumns = arrayOf("coursespot"),
				onDelete = ForeignKey.CASCADE
		),
			ForeignKey(
					entity = Run::class,
					parentColumns = arrayOf("id"),
					childColumns = arrayOf("coursebestrun")
			),
			ForeignKey(
					entity = Run::class,
					parentColumns = arrayOf("id"),
					childColumns = arrayOf("coursebestruncones")
			)],
		indices = [Index(value = ["coursespot"], name = "coursespot_idx")]
)

data class Course(
		@PrimaryKey(autoGenerate = true) var id: Int = 0,
		var name: String = "",
		var coursespot: Int = 0,
		var penalty: Int = 1,
		var coursebestrun: Int? = null,
		var coursebestruncones: Int? = null,
		var serverId: Int? = null,
		var desc: String? = null
): Parcelable

@Parcelize
data class CourseRawInfo(
		/*val id: Int,*/
		var name: String,
		var desc: String,
		/*var serverId: Int,*/
		var penalty: Int
): Parcelable
{
	companion object {
		fun fromCourse(c: Course): CourseRawInfo {
			//return CourseRawInfo(c.id, c.name, if(c.desc!= null) c.desc!! else "", if(c.serverId!= null) c.serverId!! else 0, c.penalty)
			return CourseRawInfo(c.name, if(c.desc!= null) c.desc!! else "", c.penalty)
		}
	}
}


class CourseInfo(
		@Embedded
		var course: Course,
		@Relation(parentColumn = "coursebestrun", entityColumn = "id", entity = Run::class)
		var bestRun: List<Run>,
		@Relation(parentColumn = "coursebestruncones", entityColumn = "id", entity = Run::class)
		var bestRunCones: List<Run>,
		@Relation(parentColumn = "coursespot", entityColumn = "id", entity = Spot::class)
		var spotList: List<Spot>
) {
	fun bestRun(): Run? {
		return if (bestRun.count() > 0) bestRun[0] else null
	}

	fun bestRunCones(): Run? {
		return if (bestRunCones.count() > 0) bestRunCones[0] else null
	}

	fun spot(): Spot {
		return spotList[0]
	}

	fun penVal(): Float {
		return 0.1f * course.penalty
	}
}

@Dao
interface CourseInfoDao {
	@Query("SELECT created FROM time WHERE timecourse=:courseId AND created<:borderTime ORDER BY created DESC LIMIT 1")
	fun getPrevRunDate(courseId: Int, borderTime: LocalDateTime): LocalDateTime?

	@Query("SELECT created FROM time WHERE timecourse=:courseId AND created>:borderTime ORDER BY created LIMIT 1")
	fun getNextRunDate(courseId: Int, borderTime: LocalDateTime): LocalDateTime?

	@Transaction
	@Query("SELECT * FROM course WHERE id=:courseId")
	fun getCourseInfo(courseId: Int): CourseInfo

	@Query("SELECT * FROM course WHERE id=:courseId")
	fun getCourse(courseId: Int): Course

	@Query("SELECT * FROM course")
	fun getAllCourses(): List<Course>

	//TODO add course timezone
	@Query("SELECT strftime(\"%m-%d-%Y\", created, 'unixepoch') AS date_col from time where timecourse=:courseId GROUP by date_col order by created desc;")
	fun getSessions(courseId: Int): List<String>

	@Query("UPDATE course SET serverId=:srvId WHERE id=:courseId")
	fun setSrvId(courseId: Int, srvId: Int)

	@Query("UPDATE course SET serverId=null WHERE id=:courseId")
	fun unpublishCourse(courseId: Int)

	@Update
	fun update(course: Course)

	@Insert
	fun insert(courseInfo: Course)

	@Delete
	fun delete(courseInfo: Course)

	@Delete
	fun deleteAll(courseInfo: List<Course>)
}
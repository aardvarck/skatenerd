package com.bess.skatenerd

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.migration.Migration
import com.bess.skatenerd.dbModel.*
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId


class Converters {
	@TypeConverter
	fun fromTimestamp(value: Long): LocalDateTime {
		return LocalDateTime.ofInstant(Instant.ofEpochSecond(value), ZoneId.systemDefault())
	}

	@TypeConverter
	fun dateToTimestamp(date: LocalDateTime): Long {
		return date.atZone(ZoneId.systemDefault()).toEpochSecond()
	}
}

@Database(
		entities = [Run::class, Spot::class, Course::class, Unparsed::class, Label::class, Place::class, User::class],
		version = 18
)
@TypeConverters(Converters::class)
abstract class SkatenerdDatabase : RoomDatabase() {
	abstract fun runInfoDao(): RunInfoDao
	abstract fun spotInfoDao(): SpotInfoDao
	abstract fun courseInfoDao(): CourseInfoDao
	abstract fun rawDao(): RawDao
	abstract fun unparsedDao(): UnparsedDao
	abstract fun labelDao(): LabelDao
	abstract fun placeDao(): PlaceInfoDao
	abstract fun userDao(): UserInfoDao

	companion object {
		val MIGRATION_4_5 = object : Migration(4, 5) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("BEGIN TRANSACTION;")

				database.execSQL("""CREATE TABLE "time" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"s" INTEGER NOT NULL, "ms" INTEGER NOT NULL, "cones" INTEGER NOT NULL,
						"timecourse" INTEGER NOT NULL REFERENCES course(id) ON DELETE CASCADE,
						"created" INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP)""")

				database.execSQL("INSERT INTO time(id, s, ms, timecourse, cones, created) SELECT _id, time_s, time_ms, cour, cones, strftime('%s',created) FROM times;")
				database.execSQL("DROP TABLE times;")

				database.execSQL("""CREATE TABLE "course" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"name" TEXT NOT NULL, "penalty" INTEGER NOT NULL,
						"coursespot" INTEGER NOT NULL REFERENCES spot(id) ON DELETE CASCADE)""")

				database.execSQL("INSERT INTO course(id, name, penalty, coursespot) SELECT _id, name, pen, sp FROM cources;")
				database.execSQL("DROP TABLE cources;")

				database.execSQL("""CREATE TABLE "spot" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"name" TEXT NOT NULL)""")

				database.execSQL("INSERT INTO spot(id, name) SELECT _id, name FROM spots;")
				database.execSQL("DROP TABLE spots;")

				database.execSQL("COMMIT;")
			}
		}
		val MIGRATION_5_6 = object : Migration(5, 6) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("BEGIN TRANSACTION;")

				database.execSQL("""CREATE INDEX "timecourse_idx" ON "time" ("timecourse");""")
				database.execSQL("""CREATE INDEX "coursespot_idx" ON "course" ("coursespot");""")

				database.execSQL("COMMIT;")
			}
		}
		val MIGRATION_6_7 = object : Migration(6, 7) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("BEGIN TRANSACTION;")
				database.execSQL("PRAGMA foreign_keys=OFF")

				database.execSQL("""CREATE TABLE "course_tmp" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"name" TEXT NOT NULL, "penalty" INTEGER NOT NULL,
						"coursespot" INTEGER NOT NULL REFERENCES spot(id) ON DELETE CASCADE,
						"coursebestrun" INTEGER REFERENCES time(id),
						"coursebestruncones" INTEGER REFERENCES time(id))""")

				database.execSQL("INSERT INTO course_tmp(id, name, penalty, coursespot) SELECT id, name, penalty, coursespot FROM course;")
				database.execSQL("DROP TABLE course;")
				database.execSQL("ALTER TABLE course_tmp RENAME TO course")
				database.execSQL("""CREATE INDEX "coursespot_idx" ON "course" ("coursespot");""")

				database.execSQL("PRAGMA foreign_key_check")
				database.execSQL("COMMIT;")
				database.execSQL("PRAGMA foreign_keys=ON")
			}
		}
		val MIGRATION_7_8 = object : Migration(7, 8) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("""CREATE TABLE "unparsed" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"data" TEXT NOT NULL,
						"unparsedcourse" INTEGER NOT NULL REFERENCES course(id) ON DELETE CASCADE,
						"created" INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP)""")

				database.execSQL("""CREATE INDEX "unparsedcourse_idx" ON "unparsed" ("unparsedcourse");""")

				database.execSQL("COMMIT;")
			}
		}
		val MIGRATION_8_9 = object : Migration(8, 9) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("BEGIN TRANSACTION;")
				database.execSQL("PRAGMA foreign_keys=OFF")

				database.execSQL("""CREATE TABLE "unparsed_tmp" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"data" TEXT NOT NULL,
						"unparsedcourse" INTEGER REFERENCES course(id) ON DELETE CASCADE,
						"created" INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP)""")

				database.execSQL("""DROP INDEX "unparsedcourse_idx";""")
				database.execSQL("INSERT INTO unparsed_tmp(id, data, unparsedcourse, created) SELECT id, data, unparsedcourse, created FROM unparsed;")
				database.execSQL("DROP TABLE unparsed;")
				database.execSQL("ALTER TABLE unparsed_tmp RENAME TO unparsed")
				database.execSQL("""CREATE INDEX "unparsedcourse_idx" ON "unparsed" ("unparsedcourse");""")

				database.execSQL("PRAGMA foreign_key_check")
				database.execSQL("COMMIT;")
				database.execSQL("PRAGMA foreign_keys=ON")
			}
		}
		val MIGRATION_9_10 = object : Migration(9, 10) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("BEGIN TRANSACTION;")
				database.execSQL("PRAGMA foreign_keys=OFF")

				database.execSQL("""CREATE TABLE "label" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"data" TEXT NOT NULL,
						"labelcourse" INTEGER REFERENCES course(id) ON DELETE CASCADE,
						"created" INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP)""")

				database.execSQL("""CREATE INDEX "labelcourse_idx" ON "label" ("labelcourse");""")

				database.execSQL("""CREATE TABLE "time_tmp" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"s" INTEGER NOT NULL, "ms" INTEGER NOT NULL, "cones" INTEGER NOT NULL,
						"timecourse" INTEGER NOT NULL REFERENCES course(id) ON DELETE CASCADE,
						"timelabel" INTEGER REFERENCES label(id) ON DELETE CASCADE,
						"created" INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP)""")

				database.execSQL("INSERT INTO time_tmp(id, s, ms, timecourse, cones, created) SELECT id, s, ms, timecourse, cones, created FROM time;")
				database.execSQL("DROP TABLE time;")
				database.execSQL("ALTER TABLE time_tmp RENAME TO time")
				database.execSQL("""CREATE INDEX "timecourse_idx" ON "time" ("timecourse");""")

				database.execSQL("PRAGMA foreign_key_check")
				database.execSQL("COMMIT;")
				database.execSQL("PRAGMA foreign_keys=ON")
			}
		}

		val MIGRATION_10_11 = object : Migration(10, 11) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("BEGIN TRANSACTION;")
				database.execSQL("PRAGMA foreign_keys=OFF")

				database.execSQL("""CREATE TABLE "label_tmp" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"data" TEXT NOT NULL,
						"labelcourse" INTEGER REFERENCES course(id),
						"created" INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP)""")

				database.execSQL("INSERT INTO label_tmp SELECT * FROM label;")
				database.execSQL("DROP TABLE label;")
				database.execSQL("ALTER TABLE label_tmp RENAME TO label")
				database.execSQL("""CREATE INDEX "labelcourse_idx" ON "label" ("labelcourse");""")

				database.execSQL("""CREATE TABLE "time_tmp" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"s" INTEGER NOT NULL, "ms" INTEGER NOT NULL, "cones" INTEGER NOT NULL,
						"timecourse" INTEGER NOT NULL REFERENCES course(id) ON DELETE CASCADE,
						"timelabel" INTEGER REFERENCES label(id),
						"created" INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP)""")

				database.execSQL("INSERT INTO time_tmp SELECT * FROM time;")
				database.execSQL("DROP TABLE time;")
				database.execSQL("ALTER TABLE time_tmp RENAME TO time")
				database.execSQL("""CREATE INDEX "timecourse_idx" ON "time" ("timecourse");""")

				database.execSQL("PRAGMA foreign_key_check")
				database.execSQL("COMMIT;")
				database.execSQL("PRAGMA foreign_keys=ON")
			}
		}

		val MIGRATION_11_12 = object : Migration(11, 12) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("UPDATE time SET ms=ms*10;")
			}
		}

		val MIGRATION_12_13 = object : Migration(12, 13) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("ALTER TABlE spot ADD serverId INTEGER;")
				database.execSQL("ALTER TABlE course ADD serverId INTEGER;")
			}
		}

		val MIGRATION_13_14 = object : Migration(13, 14) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("ALTER TABlE spot ADD 'desc' TEXT;")
				database.execSQL("ALTER TABlE course ADD 'desc' TEXT;")

				database.execSQL("""CREATE TABLE "place" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"desc" TEXT NOT NULL, "lon" REAL NOT NULL, "lat" REAL NOT NULL,
						"spotId" INTEGER NOT NULL REFERENCES spot(id))""")
				database.execSQL("""CREATE INDEX "spotId_idx" ON "place" ("spotId");""")
			}
		}

		val MIGRATION_14_15 = object : Migration(14, 15) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("BEGIN TRANSACTION;")
				database.execSQL("PRAGMA foreign_keys=OFF")

				database.execSQL("""CREATE TABLE "place_tmp" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"desc" TEXT NOT NULL, "lon" REAL NOT NULL, "lat" REAL NOT NULL,
						"spotId" INTEGER NOT NULL REFERENCES spot(id) ON DELETE CASCADE)""")

				database.execSQL("INSERT INTO place_tmp SELECT * FROM place;")
				database.execSQL("DROP TABLE place;")
				database.execSQL("ALTER TABLE place_tmp RENAME TO place")
				database.execSQL("""CREATE INDEX "spotId_idx" ON "place" ("spotId");""")

				database.execSQL("COMMIT;")
				database.execSQL("PRAGMA foreign_keys=ON")
			}
		}

		val MIGRATION_15_16 = object : Migration(15, 16) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("BEGIN TRANSACTION;")
				database.execSQL("PRAGMA foreign_keys=OFF")

				database.execSQL("""CREATE TABLE "label_tmp" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"data" TEXT NOT NULL,
						"labelcourse" INTEGER REFERENCES course(id) ON DELETE CASCADE,
						"created" INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP)""")

				database.execSQL("INSERT INTO label_tmp SELECT * FROM label;")
				database.execSQL("DROP TABLE label;")
				database.execSQL("ALTER TABLE label_tmp RENAME TO label")
				database.execSQL("""CREATE INDEX "labelcourse_idx" ON "label" ("labelcourse");""")

				database.execSQL("COMMIT;")
				database.execSQL("PRAGMA foreign_keys=ON")
			}
		}

		val MIGRATION_16_17 = object : Migration(16, 17) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("BEGIN TRANSACTION;")
				database.execSQL("PRAGMA foreign_keys=OFF")

				database.execSQL("""CREATE TABLE "user" (
						"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
						"name" TEXT NOT NULL,
						"email" TEXT NOT NULL,
						"serverId" INTEGER,
						"logged" INTEGER NOT NULL)""")
				database.execSQL("INSERT INTO user (id, name, email, serverId, logged) VALUES (1, \"unnamed\" , \"no@e.mail\", NULL, 1);")

				database.execSQL("ALTER TABlE time ADD COLUMN userid INTEGER NOT NULL REFERENCES user(id) DEFAULT 1;")
				database.execSQL("ALTER TABlE time ADD COLUMN serverId INTEGER;")

				database.execSQL("ALTER TABlE unparsed ADD COLUMN userid INTEGER NOT NULL REFERENCES user(id) DEFAULT 1;")

				database.execSQL("COMMIT;")
				database.execSQL("PRAGMA foreign_keys=ON")
			}
		}

		val MIGRATION_17_18 = object : Migration(17, 18) {
			override fun migrate(database: SupportSQLiteDatabase) {
				database.execSQL("BEGIN TRANSACTION;")
				database.execSQL("ALTER TABlE user ADD COLUMN gname TEXT;")
				database.execSQL("ALTER TABlE user ADD COLUMN fname TEXT;")
				database.execSQL("COMMIT;")
			}
		}
	}
}
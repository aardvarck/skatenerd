package com.bess.skatenerd.dbModel

import androidx.room.*
import org.threeten.bp.LocalDateTime
import androidx.sqlite.db.SupportSQLiteQuery
import androidx.lifecycle.LiveData
import androidx.room.RawQuery
import androidx.room.Dao

@Entity(
		tableName = "label",
		foreignKeys = [ForeignKey(onDelete = ForeignKey.CASCADE,
				entity = Course::class, parentColumns = arrayOf("id"),
				childColumns = arrayOf("labelcourse")
		)],
		indices = [Index(value = ["labelcourse"], name = "labelcourse_idx")]
)
data class Label(
		@PrimaryKey(autoGenerate = true) var id: Int,
		val data: String,
		val labelcourse: Int?,
		val created: LocalDateTime
)

@Dao
interface LabelDao {
	@Query("SELECT * FROM label WHERE id = :labId")
	fun getLabel(labId: Int): Label

	@Query("SELECT * FROM label WHERE labelcourse = :courseId")
	fun getLabels(courseId: Int): List<Label>

	@Query("SELECT * FROM label WHERE labelcourse=:courseId AND created>=:startTime AND created<=:endTime")
	fun getLabelsForDate(
			courseId: Int,
			startTime: LocalDateTime,
			endTime: LocalDateTime
	): List<Label>

	@Insert
	fun insert(unp: Label): Long

	@Delete
	fun delete(unp: Label)
}
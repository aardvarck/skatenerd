package com.bess.skatenerd.dbModel

class PrintableCourseInfo {
	var courseName: String? = null
	var spotName: String? = null
	var courseId: Int = 0
	var spotId: Int = 0
	var penalty: Int = 0
	var bestTime: Int = 0
	var bestTimeWithCones: Int = 0
}
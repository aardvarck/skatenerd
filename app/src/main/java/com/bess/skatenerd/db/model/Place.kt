package com.bess.skatenerd.dbModel

import android.os.Parcelable
import androidx.room.*
import androidx.room.Embedded
import androidx.room.Dao
import androidx.room.ForeignKey.CASCADE
import kotlinx.parcelize.Parcelize


@Parcelize
@Entity(
		tableName = "place",
		foreignKeys = [ForeignKey(onDelete = CASCADE,
				entity = Spot::class, parentColumns = arrayOf("id"),
				childColumns = arrayOf("spotId")
		)],
		indices = [Index(value = ["spotId"], name = "spotId_idx")]
)
data class Place(
		@PrimaryKey(autoGenerate = true) var id: Int,
		var desc: String = "",
		var lat: Double = 0.0,
		var lon: Double = 0.0,
		var spotId: Int = 0
): Parcelable

class PlaceInfo(
		@Embedded
		var place: Place,
		@Relation(parentColumn = "spotId", entityColumn = "id", entity = Spot::class)
		var spot: List<Spot>
) {
	fun spot(): Spot? {
		return if (spot.count() > 0) spot[0] else null
	}
}

@Dao
interface PlaceInfoDao {
	@Query("SELECT * FROM place WHERE spotId=:spotId")
	fun getPlaceForSpot(spotId: Int): Place

	@Query("SELECT * FROM place")
	fun getPlaces(): List<PlaceInfo>

	@Insert
	fun insert(place: Place)

	@Delete
	fun delete(place: Place)

	@Query("DELETE FROM place WHERE spotId=:spotId")
	fun removeFrom(spotId: Int)

	@Update
	fun update(place: Place)
}



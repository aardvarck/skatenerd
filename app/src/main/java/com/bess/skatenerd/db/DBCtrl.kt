package com.bess.skatenerd.db

import android.content.Context
import android.database.DatabaseUtils
import android.database.sqlite.SQLiteDatabase
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.RoomMasterTable.TABLE_NAME
import androidx.sqlite.db.SupportSQLiteDatabase
import com.bess.skatenerd.SkatenerdDatabase
import com.bess.skatenerd.dbModel.*
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter


/**
 * Created by bess on 18.06.2014.
 */
class DBCtrl(context: Context) {
	companion object {
		@Volatile private var INSTANCE: DBCtrl? = null
		fun createInstance(context: Context): DBCtrl = INSTANCE ?: synchronized(this) {INSTANCE ?: buildHelper(context).also { INSTANCE = it }}
		private fun buildHelper(context: Context) = DBCtrl(context)
		fun get(): DBCtrl {return INSTANCE!!}
	}

	var usrClb: RoomDatabase.Callback = object : RoomDatabase.Callback() {
		override fun onCreate(db: SupportSQLiteDatabase) {
			val crs = db.query("select count() from user")
			crs.moveToFirst()
			val usrCount = crs.getInt(0)
			crs.close()
			if( usrCount == 0 ){
				db.execSQL("INSERT INTO user (id, name, email, serverId, logged) VALUES (1, \"unnamed\" , \"no@e.mail\", NULL, 1);")
			}
		}
	}

	private val formatterYear = DateTimeFormatter.ofPattern("MM-dd-yyyy")
	private var mCurUserId: Int = 1
	private var roomDb: SkatenerdDatabase =
			Room.databaseBuilder(context, SkatenerdDatabase::class.java, "skatedb")
					.allowMainThreadQueries()
					.addMigrations(SkatenerdDatabase.MIGRATION_4_5)
					.addMigrations(SkatenerdDatabase.MIGRATION_5_6)
					.addMigrations(SkatenerdDatabase.MIGRATION_6_7)
					.addMigrations(SkatenerdDatabase.MIGRATION_7_8)
					.addMigrations(SkatenerdDatabase.MIGRATION_8_9)
					.addMigrations(SkatenerdDatabase.MIGRATION_9_10)
					.addMigrations(SkatenerdDatabase.MIGRATION_10_11)
					.addMigrations(SkatenerdDatabase.MIGRATION_11_12)
					.addMigrations(SkatenerdDatabase.MIGRATION_12_13)
					.addMigrations(SkatenerdDatabase.MIGRATION_13_14)
					.addMigrations(SkatenerdDatabase.MIGRATION_14_15)
					.addMigrations(SkatenerdDatabase.MIGRATION_15_16)
					.addMigrations(SkatenerdDatabase.MIGRATION_16_17)
					.addMigrations(SkatenerdDatabase.MIGRATION_17_18)
					.setJournalMode(RoomDatabase.JournalMode.TRUNCATE)
					.addCallback(usrClb)
					.build()

	fun setUserSrvId(email: String, id: Int){
		roomDb.userDao().setSrvId(email, id)
	}

	fun setDefaultUser() {
		mCurUserId = 1;
	}

	fun setUser(user: User){
		val usr = roomDb.userDao().getUser(user.email)
		mCurUserId = usr?.id ?: roomDb.userDao().insert(user).toInt()

		roomDb.runInfoDao().setUnnamed(mCurUserId)
		roomDb.unparsedDao().setUnnamed(mCurUserId)
	}

	fun getAllPlaces(): List<PlaceInfo> {
		return roomDb.placeDao().getPlaces()
	}

	fun setSpotPlace(spotId: Int, place: Place) {
		place.spotId = spotId
		roomDb.placeDao().removeFrom(spotId)
		roomDb.placeDao().insert(place)
	}

	fun removePlaceFromSpot(spotId: Int) {
		roomDb.placeDao().removeFrom(spotId)
	}

	fun removePlace(place: Place) {
		roomDb.placeDao().delete(place)
	}

	fun getCourseLabels(courseId: Int): List<Label> {
		return roomDb.labelDao().getLabels(courseId)
	}

	fun getCourseDates(courseId: Int): List<LocalDateTime>{

		val lst = mutableListOf<LocalDateTime>()
		for(s in roomDb.courseInfoDao().getSessions(courseId))
			lst.add(LocalDate.parse(s, formatterYear).atStartOfDay())
		return lst
	}

	fun getAllCourses(): List<Course> {
		return roomDb.courseInfoDao().getAllCourses()
	}

	fun getAllSpotsCourses(): List<SpotWithCourses> {
		return roomDb.spotInfoDao().getAllSpotsCourses()
	}

	fun getSpotInfo(spotId: Int): Spot{
		return roomDb.spotInfoDao().getSpotInfo(spotId)
	}

	fun getPlaceForSpot(spotId: Int): Place{
		return roomDb.placeDao().getPlaceForSpot(spotId)
	}

	fun getAllCourseDateRuns(courseId: Int): Map<LocalDate, MutableList<Run>> {
		val runs = roomDb.runInfoDao().getCourseRuns(courseId)

		val info = mutableMapOf<LocalDate, MutableList<Run>>()
		for (r in runs) {
			val localDate = r.created.toLocalDate()
			if (!info.containsKey(localDate)) {
				info[localDate] = mutableListOf()
			}

			info[localDate]!!.add(r)
		}
		return info
	}

	fun getLastRun(): Run? {
		return roomDb.runInfoDao().getLastRun()
	}

	fun getRun(rid: Int): RunInfo? {
		return roomDb.runInfoDao().getRun(rid)
	}

	fun getPrevCourseDate(courseId: Int, date: LocalDate): LocalDate? {
		val prevDay = date.minusDays(1)
		return roomDb.courseInfoDao().getPrevRunDate(courseId, prevDay.atTime(23, 59))
				?.toLocalDate()
	}

	fun getNextCourseDate(courseId: Int, date: LocalDate): LocalDate? {
		val nextDay = date.plusDays(1)
		return roomDb.courseInfoDao().getNextRunDate(courseId, nextDay.atTime(0, 0))?.toLocalDate()
	}

	fun getCourseInfo(courseId: Int): CourseInfo {
		return roomDb.courseInfoDao().getCourseInfo(courseId)
	}

	fun getUnparsedForCourse(courseId: Int, date: LocalDate): List<Unparsed> {
		return roomDb.unparsedDao()
				.getUnparsedForDate(courseId, date.atTime(0, 0), date.atTime(23, 59))
	}

	fun removeUnparsed(unparsed: Unparsed) {
		roomDb.unparsedDao().delete(unparsed)
	}

	fun addUnparsed(courseId: Int?, text: String) {
		val unp = Unparsed(0, text, courseId, LocalDateTime.now(), mCurUserId)
		roomDb.unparsedDao().insert(unp)
	}

	fun getAllUnparsed(): List<Unparsed> {
		return roomDb.unparsedDao().getAllUnparsed()
	}

	fun getUnparsedCount(): Int {
		return roomDb.unparsedDao().count()
	}

	fun setUnparsedCourse(unpId: Int, courseId: Int) {
		roomDb.unparsedDao().updateCourse(unpId, courseId)
	}


//	for REAL time column migration (may be in the future)
//	fun genRealTimes(){
//		val cursor = database!!.rawQuery("SELECT _id, time_s, time_ms FROM times", null	)
//		cursor.moveToFirst()
//		while (!cursor.isAfterLast) {
//			val id = cursor.getInt(0)
//			val tm: Float = cursor.getInt(1) + 0.01f * cursor.getInt(2)
//
//
//			val args = arrayOfNulls<String>(1)
//			args[0] = id.toString()
//			val cv = ContentValues(1)
//			cv.put("time", tm)
//			val genTm = database!!.update(MyDatabaseHelper.TABLE_TIMES, cv, "_id=?", args).toLong()
//
//			cursor.moveToNext()
//		}
//	}

	fun getCourseRunsForDate(courseId: Int, date: LocalDate): List<RunInfo> {
		return roomDb.runInfoDao()
				.getCourseRunForDate(courseId, date.atTime(0, 0), date.atTime(23, 59))
	}

	fun getCourseRuns(courseId: Int): List<Run> {
		return roomDb.runInfoDao().getCourseRuns(courseId)
	}

	fun addSpot(spot: Spot, place: Place?) {
		spot.id = 0
		val spotId = roomDb.spotInfoDao().insert(spot).toInt()
		if (place != null) {
			place.id = 0
			place.spotId = spotId
			roomDb.placeDao().insert(place)
		}
	}

	fun addTime(tm: RunTime, courseId: Int, cones: Int, date: LocalDateTime, label: String) {
		var labelId: Int? = null
		if (!label.isEmpty()) {
			labelId = roomDb.labelDao().insert(Label(0, label, courseId, date)).toInt()
		}

		val runInfo = Run(0, tm.s, tm.ms, cones, courseId, date, labelId, mCurUserId)
		roomDb.runInfoDao().insert(runInfo)
		updateCourseBestTimes(courseId)
	}

	fun editTime(rid: Int, tm: RunTime, courseId: Int, cones: Int, label: String) {
		val ri = roomDb.runInfoDao().getRun(rid)
		ri.run.s = tm.s
		ri.run.ms = tm.ms
		ri.run.cones = cones

		var labelId: Int? = null
		if (!label.isEmpty()) {
			labelId = roomDb.labelDao().insert(Label(0, label, courseId, ri.run.created)).toInt()
		}

		val oldLabel = ri.run.timelabel
		ri.run.timelabel = labelId
		roomDb.runInfoDao().update(ri.run)

		if (oldLabel != null) {
			val lab = roomDb.labelDao().getLabel(oldLabel)
			roomDb.labelDao().delete(lab)
		}

		updateCourseBestTimes(courseId)
	}

	fun createNewSpot(spot: Spot) {
		roomDb.spotInfoDao().insert(spot)
	}

	fun renameSpot(spot: Spot) {
		roomDb.spotInfoDao().update(spot)
	}

	fun setSpotSrvId(spotId: Int, srvId: Int) {
		roomDb.spotInfoDao().setSrvId(spotId, srvId)
	}

	fun setCourseSrvId(courseId: Int, srvId: Int) {
		roomDb.courseInfoDao().setSrvId(courseId, srvId)
	}

	fun removeSpot(spotId: Int) {
		val args = arrayOfNulls<String>(1)
		args[0] = spotId.toString()

		val spi = roomDb.spotInfoDao().getSpotInfoWithCourses(spotId)

		roomDb.spotInfoDao().delete(spi.spot!!)
		roomDb.courseInfoDao().deleteAll(spi.courses!!)
	}

	fun removeCourse(courseId: Int) {
		val course = roomDb.courseInfoDao().getCourse(courseId)
		roomDb.courseInfoDao().delete(course)
	}

	fun unpublishCourse(courseId: Int){
		roomDb.courseInfoDao().unpublishCourse(courseId)
	}

	fun removeRun(runId: Int) {
		val run = roomDb.runInfoDao().getRun(runId)
		val course = roomDb.courseInfoDao().getCourse(run.run.timecourse)
		var courseUpdate = false
		if (course.coursebestrun == runId) {
			course.coursebestrun = null
			courseUpdate = true
		}
		if (course.coursebestruncones == runId) {
			course.coursebestruncones = null
			courseUpdate = true
		}

		if (courseUpdate) roomDb.courseInfoDao().update(course)

		val lab = run.label()
		if (lab != null) {
			run.run.timelabel = null
			roomDb.runInfoDao().update(run.run)
			roomDb.labelDao().delete(lab)
		}

		roomDb.runInfoDao().delete(run.run)

		updateCourseBestTimes(run.run.timecourse)
	}

	fun createNewCourse(course: Course) {
		roomDb.courseInfoDao().insert(course)
	}

	fun setCourseDetails(course: Course) {
		roomDb.courseInfoDao().update(course)
	}

	private fun updateCourseBestTimes(courseId: Int) {
		val ci = roomDb.courseInfoDao().getCourse(courseId)
		val runs = roomDb.runInfoDao().getCourseRuns(courseId)
		if (runs.count() == 0) {
			ci.coursebestrun = null
			ci.coursebestruncones = null
		} else {
			val conesPenalty = 0.1f * ci.penalty
			var bt = runs[0].rawTime(0.0f)
			var btc = runs[0].rawTime(conesPenalty)
			for (i in 1 until runs.count()) {
				val curt = runs[i].rawTime(0.0f)
				val curtc = runs[i].rawTime(conesPenalty)
				if (curt < bt) {
					ci.coursebestrun = runs[i].id
					bt = curt
				}
				if (curtc < btc) {
					ci.coursebestruncones = runs[i].id
					btc = curtc
				}
			}
		}
		roomDb.courseInfoDao().update(ci)
	}

	fun closeDb() { roomDb.close() }

	fun upgradeDb(context: Context) {
		roomDb = Room.databaseBuilder(context, SkatenerdDatabase::class.java, "skatedb")
				.allowMainThreadQueries()
				.addMigrations(SkatenerdDatabase.MIGRATION_4_5)
				.addMigrations(SkatenerdDatabase.MIGRATION_5_6)
				.addMigrations(SkatenerdDatabase.MIGRATION_6_7)
				.addMigrations(SkatenerdDatabase.MIGRATION_7_8)
				.addMigrations(SkatenerdDatabase.MIGRATION_8_9)
				.addMigrations(SkatenerdDatabase.MIGRATION_9_10)
				.addMigrations(SkatenerdDatabase.MIGRATION_10_11)
				.addMigrations(SkatenerdDatabase.MIGRATION_11_12)
				.addMigrations(SkatenerdDatabase.MIGRATION_12_13)
				.addMigrations(SkatenerdDatabase.MIGRATION_13_14)
				.addMigrations(SkatenerdDatabase.MIGRATION_14_15)
				.addMigrations(SkatenerdDatabase.MIGRATION_15_16)
				.addMigrations(SkatenerdDatabase.MIGRATION_16_17)
				.addMigrations(SkatenerdDatabase.MIGRATION_17_18)
				.setJournalMode(RoomDatabase.JournalMode.TRUNCATE)
				//.fallbackToDestructiveMigration()
				.addCallback(usrClb)
				.build()
	}
}

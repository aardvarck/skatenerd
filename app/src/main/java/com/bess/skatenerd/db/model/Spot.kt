package com.bess.skatenerd.dbModel

import android.os.Parcelable
import androidx.room.*
import androidx.room.Embedded
import androidx.room.Dao
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "spot")
data class Spot(
		@PrimaryKey(autoGenerate = true) var id: Int = 0,
		var name: String = "",
		var serverId: Int? = null,
		var desc: String? = null
): Parcelable

class SpotWithCourses (
	@Embedded
	var spot: Spot? = null,
	@Relation(parentColumn = "id", entityColumn = "coursespot", entity = Course::class)
	var courses: List<Course>? = null,
	@Relation(parentColumn = "id", entityColumn = "spotId", entity = Place::class)
	var place: List<Place>)
{
	fun place(): Place? {
		return if (place.count() > 0) place[0] else null
	}
}

@Dao
interface SpotInfoDao {
	@Query("SELECT * FROM spot")
	fun getSpots(): List<Spot>

	@Transaction
	@Query("SELECT * FROM spot")
	fun getAllSpotsCourses(): List<SpotWithCourses>

	@Transaction
	@Query("SELECT * FROM spot WHERE id=:spotId")
	fun getSpotInfoWithCourses(spotId: Int): SpotWithCourses

	@Query("SELECT * FROM spot WHERE id=:spotId")
	fun getSpotInfo(spotId: Int): Spot

	@Query("UPDATE spot SET serverId=:srvId WHERE id=:spotId")
	fun setSrvId(spotId: Int, srvId: Int)

	@Insert
	fun insert(spotInfo: Spot): Long

	@Delete
	fun delete(spotInfo: Spot)

	@Update
	fun update(spotInfo: Spot)
}



package com.bess.skatenerd.dbModel

import android.os.Parcelable
import androidx.room.*
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable
import org.threeten.bp.LocalDateTime

@Serializable
@Parcelize
@Entity(tableName = "user")
data class User(
		@PrimaryKey(autoGenerate = true) var id: Int = 0,
		var name: String = "",
		var email: String = "",
		var serverId: Int? = null,
		var logged: Boolean = false,
		var fname: String? = "",
		var gname: String? = ""
): Parcelable
{
	fun shortName(): String{
		return if(gname != null) gname?.first().toString() + ". " + fname!!
		else return fname!!
	}
}

@Dao
interface UserInfoDao {
	@Query("UPDATE user SET serverId=:srvId WHERE email=:email")
	fun setSrvId(email: String, srvId: Int)

	@Query("SELECT * FROM user WHERE email=:email")
	fun getUser(email: String): User?

	@Update
	fun update(user: User)

	@Insert
	fun insert(user: User): Long

	@Delete
	fun delete(user: User)
}
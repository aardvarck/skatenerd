package com.bess.skatenerd.dbModel

import androidx.room.*
import org.threeten.bp.LocalDateTime
import androidx.sqlite.db.SupportSQLiteQuery
import androidx.lifecycle.LiveData
import androidx.room.RawQuery
import androidx.room.Dao

@Entity(
		tableName = "unparsed",
		foreignKeys = [
			ForeignKey(entity = Course::class, parentColumns = arrayOf("id"), childColumns = arrayOf("unparsedcourse"), onDelete = ForeignKey.CASCADE),
			ForeignKey(entity = User::class, parentColumns = arrayOf("id"), childColumns = arrayOf("userid"))],
		indices = [Index(value = ["unparsedcourse"], name = "unparsedcourse_idx")]
)
data class Unparsed(
		@PrimaryKey(autoGenerate = true) var id: Int,
		val data: String,
		val unparsedcourse: Int?,
		val created: LocalDateTime,
		var userid: Int
)

@Dao
interface UnparsedDao {
	@Query("SELECT * FROM unparsed ORDER BY created DESC")
	fun getAllUnparsed(): List<Unparsed>

	@Query("SELECT * FROM unparsed WHERE unparsedcourse = :courseId")
	fun getUnparsed(courseId: Int): List<Unparsed>

	@Query("SELECT * FROM unparsed WHERE unparsedcourse=:courseId AND created>=:startTime AND created<=:endTime")
	fun getUnparsedForDate(
			courseId: Int,
			startTime: LocalDateTime,
			endTime: LocalDateTime
	): List<Unparsed>

	@Query("UPDATE unparsed SET userid=:userId WHERE userid=1")
	fun setUnnamed(userId: Int)

	@Query("UPDATE unparsed SET unparsedcourse=:courseId WHERE id=:unpId")
	fun updateCourse(unpId: Int, courseId: Int)

	@Query("SELECT COUNT(*) FROM unparsed")
	fun count(): Int

	@Insert
	fun insert(unp: Unparsed)

	@Delete
	fun delete(unp: Unparsed)
}
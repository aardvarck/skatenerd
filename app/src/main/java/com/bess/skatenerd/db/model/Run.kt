package com.bess.skatenerd.dbModel

import androidx.room.*
import org.threeten.bp.LocalDateTime
import androidx.sqlite.db.SupportSQLiteQuery
import androidx.lifecycle.LiveData
import androidx.room.RawQuery
import androidx.room.Dao
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer


class RunTime(
		internal var s: Int = 0,
		internal var ms: Int = 0
) {

	fun toFloat(): Float {
		return s + 0.001f * ms
	}

	override fun toString(): String {
		return "$s:$ms"
	}
}

@Entity(
		tableName = "time",
		foreignKeys = [
			ForeignKey(entity = Course::class, parentColumns = arrayOf("id"), childColumns = arrayOf("timecourse"), onDelete = ForeignKey.CASCADE),
			ForeignKey(entity = Label::class, parentColumns = arrayOf("id"), childColumns = arrayOf("timelabel")),
			ForeignKey(entity = User::class, parentColumns = arrayOf("id"), childColumns = arrayOf("userid"))],
		indices = [Index(value = ["timecourse"], name = "timecourse_idx")]
)
data class Run(
		@PrimaryKey(autoGenerate = true) var id: Int,
		var s: Int,
		var ms: Int,
		var cones: Int,
		var timecourse: Int,
		var created: LocalDateTime,
		var timelabel: Int? = null,
		var userid: Int,
		var serverId: Int? = null
) {
	@Ignore
	fun tm(): RunTime {
		return RunTime(s, ms)
	}

	fun rawTime(pen: Float): Float {
		return tm().toFloat() + pen * cones
	}

	fun toString(countCones: Boolean, conePenalty: Float, printThPart: Int, printCones: Boolean = true): String {
		val rt = RunTime(s, ms)
		val tm = rt.toFloat() + if (countCones) cones * conePenalty else 0.0f
		val frmStr = if (printThPart == 1) "%.3f" else "%.2f"
		return frmStr.format(tm) + if (printCones && (cones > 0)) "(%s%d)".format(
				if (countCones) "" else "+",
				cones
		) else ""
	}

	fun hasThPart(): Boolean {
		return (ms % 10) != 0
	}

}

//class CompareRuns  (val countCones: Boolean, val penalty: Float){
//    companion object : Comparator<Run>{
//        override fun compare(a: Run, b: Run): Int = when {
//            return a.rawTime() - b.rawTime()
//        }
//    }
//}

class RunInfo(@Embedded	var run: Run,
		@Relation(parentColumn = "timelabel", entityColumn = "id", entity = Label::class) var label: List<Label>) {
	fun label(): Label? {
		return if (label.count() > 0) label[0] else null
	}
}

@Dao
interface RunInfoDao {
	@Query("SELECT * FROM time WHERE id = :runId")
	fun getRun(runId: Int): RunInfo

	@Query("SELECT * FROM time ORDER BY created DESC LIMIT 1")
	fun getLastRun(): Run

	@Insert
	fun insert(ri: Run)

	@Delete
	fun delete(ri: Run)

	@Update
	fun update(ri: Run)

	@Query("UPDATE time SET userid=:userId WHERE userid=1")
	fun setUnnamed(userId: Int)

	@Query("SELECT * FROM time WHERE timecourse = :courseId")
	fun getCourseRuns(courseId: Int): List<Run>

	@Query("SELECT created FROM time WHERE timecourse = :courseId")
	fun getCourseRunDates(courseId: Int): List<LocalDateTime>

	@Query("SELECT * FROM time WHERE timecourse=:courseId AND created>=:startTime AND created<=:endTime")
	fun getCourseRunForDate(courseId: Int, startTime: LocalDateTime, endTime: LocalDateTime): List<RunInfo>
}

@Dao
interface RawDao {
	@RawQuery
	fun delRun(query: SupportSQLiteQuery): Run?
}
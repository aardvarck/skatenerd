package com.bess.skatenerd

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.preference.PreferenceManager

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet

import android.view.*
import android.view.View.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController

import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.formatter.ValueFormatter
import com.google.android.material.floatingactionbutton.FloatingActionButton

import org.threeten.bp.LocalDate
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import android.widget.*
import androidx.appcompat.widget.AppCompatImageButton
import androidx.navigation.NavOptions
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.databinding.FragmentShowCourseBinding
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.*
import com.github.mikephil.charting.listener.ChartTouchListener
import com.github.mikephil.charting.listener.OnChartGestureListener
import org.threeten.bp.format.DateTimeFormatter
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView


class ShowCourseFragment : Fragment() {
	private lateinit var mChart: LineChart
	private lateinit var mList: RecyclerView
	private lateinit var mNoRunsTxtView: TextView
	private lateinit var mRunDesc: TextView
	private lateinit var mListButton: AppCompatImageButton
	private lateinit var mGraphButton: AppCompatImageButton
	private lateinit var mConesButton: FloatingActionButton

	private var mDate: LocalDate = LocalDate.now()
	private lateinit var mCourseInfo: CourseInfo
	private lateinit var mTimes: List<RunInfo>
	private var mNextDate: LocalDate? = null
	private var mPrevDate: LocalDate? = null
	private var mClickedRunInfo: RunInfoHelper? = null
	private lateinit var mChartTypeface: Typeface
	private lateinit var mUnparsed: Button
	private var mBinding: FragmentShowCourseBinding? = null
	private val binding get() = mBinding!!

	companion object {
		var showWithCones: Boolean = false
		var tableView: Boolean = false
		var printThPart: Boolean = false
	}

	fun getCourseId(): Int {
		return mCourseInfo.course.id
	}

	@SuppressLint("ClickableViewAccessibility")
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		showWithCones =	PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_cones_def",false)
		tableView =	PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_table_runs_view",false)
		printThPart = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pref_show_micro_digit",false)

		mBinding = FragmentShowCourseBinding.inflate(inflater)

		mChartTypeface = Typeface.createFromAsset(requireContext().assets, "fonts/OpenSans-Bold.ttf")

		val args = ShowCourseFragmentArgs.fromBundle(requireArguments())
		mDate = args.date

		mCourseInfo = DBCtrl.get().getCourseInfo(args.courseId)

		mNextDate = DBCtrl.get().getNextCourseDate(mCourseInfo.course.id, mDate)
		if (mNextDate == null && mDate != LocalDate.now()) mNextDate = LocalDate.now()
		mPrevDate = DBCtrl.get().getPrevCourseDate(mCourseInfo.course.id, mDate)

		return binding.root
	}

	private fun getLastTime(): RunTime {
		return if (mTimes.isEmpty()) RunTime(10, 0) else mTimes.last().run.tm()
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		val title = view.findViewById<View>(R.id.nameTxtView) as TextView
		title.text =
				getString(R.string.course_runs_title, mCourseInfo.spot().name, mCourseInfo.course.name)

		val penTxtView = view.findViewById<View>(R.id.penaltyTextView) as TextView
		penTxtView.text = getString(R.string.cone_penalty_calculated, mCourseInfo.course.penalty)

		val dateTxtView = view.findViewById<TextView>(R.id.dateTxtView)
		dateTxtView.text = mDate.format(DateTimeFormatter.ofPattern("dd LLLL yyyy"))

		val excelButton = view.findViewById<AppCompatImageButton>(R.id.excelButton)
		excelButton.setOnClickListener {
			ExportXlsHelper.exportCourse(requireContext(), requireActivity(), mCourseInfo.course, mDate)
		}

		val histButton = view.findViewById<Button>(R.id.historyButton)
		histButton.setOnClickListener {
			it.findNavController()
					.navigate(
							ShowCourseFragmentDirections.actionShowCourseFragmentToDateRunsFragment(
									mCourseInfo.course.id
							)
					)
		}

		val addButton = view.findViewById<FloatingActionButton>(R.id.addButton)
		addButton.setOnClickListener {
			val lastTime = getLastTime()
			it.findNavController().navigate(
					ShowCourseFragmentDirections.actionShowCourseFragmentToAddTimeFragment2(
							mCourseInfo.course.id,
							mDate,
							lastTime.s,
							lastTime.ms,
							false,
							false,
							showMicroDigit = printThPart
					)
			)
		}

		val micButton = view.findViewById<FloatingActionButton>(R.id.micButton)
		micButton.setOnClickListener {
			val lastTime = getLastTime()
			it.findNavController().navigate(
					ShowCourseFragmentDirections.actionShowCourseFragmentToAddTimeFragment2(
							mCourseInfo.course.id,
							mDate,
							lastTime.s,
							lastTime.ms,
							true,
							false,
							showMicroDigit = printThPart
					)
			)
		}

		mConesButton = view.findViewById(R.id.conesButton)
		mConesButton.setOnClickListener {
			showWithCones = !showWithCones
			updateData()
		}

		mListButton = view.findViewById(R.id.listButton)
		mListButton.setOnClickListener { setRunsRepresentation(true) }

		mGraphButton = view.findViewById(R.id.graphButton)
		mGraphButton.setOnClickListener { setRunsRepresentation(false) }

		val swipeListener = object : OnSwipeTouchListener() {
			override fun onSwipeRight() {
				printThPart = PreferenceManager.getDefaultSharedPreferences(context)
						.getBoolean("pref_show_micro_digit", false)
				val navOptions: NavOptions = NavOptions.Builder()
						.setEnterAnim(R.anim.enter_from_left).setExitAnim(R.anim.exit_to_right)
						.setPopUpTo(R.id.showCourseFragment, true).build()
				view.findNavController().navigate(
						ShowCourseFragmentDirections.actionShowCourseFragmentSelf(
								mCourseInfo.course.id,
								mPrevDate!!
						), navOptions
				)
			}

			override fun onSwipeLeft() {
				printThPart = PreferenceManager.getDefaultSharedPreferences(context)
						.getBoolean("pref_show_micro_digit", false)
				val navOptions: NavOptions = NavOptions.Builder()
						.setEnterAnim(R.anim.enter_from_right).setExitAnim(R.anim.exit_to_left)
						.setPopUpTo(R.id.showCourseFragment, true).build()
				view.findNavController().navigate(
						ShowCourseFragmentDirections.actionShowCourseFragmentSelf(
								mCourseInfo.course.id,
								mNextDate!!
						), navOptions
				)
			}
		}

		val mTopLinLay = view.findViewById<View>(R.id.swipeTopLayout) as LinearLayout
		mTopLinLay.setOnTouchListener(swipeListener)
		val mBottomLinLay = view.findViewById<View>(R.id.swipeBottomLayout) as LinearLayout
		mBottomLinLay.setOnTouchListener(swipeListener)

		mChart = view.findViewById<View>(R.id.chart) as LineChart
		mChart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
			override fun onValueSelected(e: Entry, h: Highlight) {
				mClickedRunInfo = e.data as RunInfoHelper
				val rInfo = mClickedRunInfo
				if (rInfo != null && (rInfo.mRun.run.created.toLocalDate() != mDate)) return
				view.findNavController().navigate(
						ShowCourseFragmentDirections.actionShowCourseFragmentToRunNotePopupFragment(
								rInfo!!.mRun.run.id,
								rInfo.toString(showWithCones),
								rInfo.mRun.label()?.id ?: -1,
								rInfo.mRun.label()?.data,
								mDate,
								mCourseInfo.course.id,
								printThPart
						)
				)
			}

			override fun onNothingSelected() {
				mClickedRunInfo = null
			}
		})

		mChart.setOnChartGestureListener(object : OnChartGestureListener {
			override fun onChartGestureEnd(
					me: MotionEvent?,
					lastPerformedGesture: ChartTouchListener.ChartGesture?
			) {
			}

			override fun onChartFling(
					me1: MotionEvent?,
					me2: MotionEvent?,
					velocityX: Float,
					velocityY: Float
			) {
			}

			override fun onChartDoubleTapped(me: MotionEvent?) {}
			override fun onChartLongPressed(me: MotionEvent?) {}
			override fun onChartSingleTapped(me: MotionEvent?) {}
			override fun onChartGestureStart(
					me: MotionEvent?,
					lastPerformedGesture: ChartTouchListener.ChartGesture?
			) {
			}

			override fun onChartScale(me: MotionEvent?, scaleX: Float, scaleY: Float) {}
			override fun onChartTranslate(me: MotionEvent?, dX: Float, dY: Float) {}
		})

		mChart.maxHighlightDistance = 20.0f

		mNoRunsTxtView = view.findViewById(R.id.noRunsText)
		mRunDesc = view.findViewById(R.id.startRunsTxtView)
		mList = view.findViewById(R.id.runList)

		mUnparsed = view.findViewById(R.id.unparsedButton)
		mUnparsed.setOnClickListener {
			it.findNavController().navigate(
					ShowCourseFragmentDirections.actionShowCourseFragmentToAddTimeFragment2(
							mCourseInfo.course.id, mDate, 0, 0, false, true, showMicroDigit = printThPart
					)
			)
		}

		setRunsRepresentation(tableView)

		val act: MainActivity = activity as MainActivity

		val addB = MaterialShowcaseView.Builder(act)
				.setTarget(addButton)
				.setDismissOnTouch(true)
				.setContentText(R.string.tip_show_course_add)
		val conesB = MaterialShowcaseView.Builder(act)
				.setTarget(binding.conesButton)
				.setDismissOnTouch(true)
				.setContentText(R.string.tip_show_course_cones)
		val topB = MaterialShowcaseView.Builder(act)
				.setTarget(binding.swipeTopLayout)
				.setDismissOnTouch(true)
				.setContentText(R.string.tip_show_course_top_swipe)
				.withRectangleShape()
		val botB = MaterialShowcaseView.Builder(act)
				.setTarget(binding.swipeBottomLayout)
				.setDismissOnTouch(true)
				.setContentText(R.string.tip_show_course_bot_swipe)
				.withRectangleShape()

		MaterialShowcaseSequence(act)
				.addSequenceItem(addB.build())
				.addSequenceItem(conesB.build())
				.addSequenceItem(topB.build())
				.addSequenceItem(botB.build())
				.singleUse(getString(R.string.tip_show_course_id))
				.start()
	}

	fun updateData() {
		mTimes = DBCtrl.get().getCourseRunsForDate(mCourseInfo.course.id, mDate)

		var needThPart = false
		mTimes.forEach { ri -> needThPart = needThPart || ri.run.hasThPart() }
		printThPart = printThPart || needThPart

		if (tableView) fillList() else fillChart()

		mConesButton.backgroundTintList = ColorStateList.valueOf(
				ContextCompat.getColor(
						requireContext(),
						if (showWithCones) R.color.notSoTransparent else R.color.transparent
				)
		)

		mNoRunsTxtView.visibility = if (mTimes.isEmpty()) View.VISIBLE else View.INVISIBLE

		val runsCount = mTimes.count()
		val dayRunsDesc = if (runsCount == 0) "-" else mTimes.first().run.created.format(
				DateTimeFormatter.ofPattern("HH:mm")
		)
		mRunDesc.text =
				String.format(resources.getString(R.string.StartDurationDesc), dayRunsDesc, runsCount)

		val unparsed = DBCtrl.get().getUnparsedForCourse(mCourseInfo.course.id, mDate)
		mUnparsed.visibility = if (unparsed.count() > 0) VISIBLE else INVISIBLE
	}

	private fun setRunsRepresentation(list: Boolean) {
		mChart.visibility = if (list) GONE else VISIBLE
		mList.visibility = if (!list) GONE else VISIBLE
		mGraphButton.setBackgroundColor(
				if (!list) ContextCompat.getColor(
						requireContext(),
						R.color.notSoTransparent
				) else ContextCompat.getColor(requireContext(), R.color.transparent)
		)
		mListButton.setBackgroundColor(
				if (list) ContextCompat.getColor(
						requireContext(),
						R.color.notSoTransparent
				) else ContextCompat.getColor(requireContext(), R.color.transparent)
		)
		tableView = list

		updateData()
	}

	private fun fillList() {
		val headers = listOf(
				getString(R.string.hist_head_date),
				getString(R.string.hist_head_best),
				getString(R.string.hist_head_count)
		)
		mList.adapter = ShowCourseRunsRecyclerViewAdapter(
				mTimes.reversed(),
				headers,
				showWithCones,
				mCourseInfo.penVal(),
				printThPart
		)
	}

	private fun fillChart() {
		//<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
		val bestRun: Run? = mCourseInfo.bestRun()
		val bestRunCones: Run? = mCourseInfo.bestRunCones()

		val lineData = LineData()
		var idx = 1.0f
		if (!showWithCones && bestRun != null && bestRun.created.toLocalDate() != mDate) {
			val ds = makeRunsDataset(
					listOf(RunInfo(bestRun, emptyList())),
					idx,
					Color.RED,
					Color.RED,
					true,
					false
			)
			lineData.addDataSet(ds)
			idx += ds.entryCount
		}

		if (showWithCones && bestRunCones != null && bestRunCones.created.toLocalDate() != mDate) {
			val ds = makeRunsDataset(
					listOf(RunInfo(bestRunCones, emptyList())),
					idx,
					Color.RED,
					Color.RED,
					false,
					true
			)
			lineData.addDataSet(ds)
			idx += ds.entryCount
		}

		val runsDs = makeRunsDataset(
				mTimes,
				idx,
				Color.BLUE,
				ContextCompat.getColor(requireContext(), R.color.colorPrimary),
				false,
				false
		)
		runsDs.color = ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)
		runsDs.lineWidth = 5.0f
		lineData.addDataSet(runsDs)
		idx += runsDs.entryCount

		var maxY = 0.0f
		var maxYWithCones = 0.0f
		var minY = 999.9f
		val tmpList = mTimes.map { it.run }.toMutableList()
		if (bestRun != null) tmpList.add(bestRun)
		if (bestRunCones != null) tmpList.add(bestRunCones)
		for (r in tmpList) {
			val floatTime = r.tm().toFloat()
			val runConePenalty = mCourseInfo.penVal() * r.cones
			maxY = maxOf(maxY, floatTime)
			maxYWithCones = maxOf(maxYWithCones, floatTime + runConePenalty)
			minY = minOf(minY, floatTime)
		}

		val realMaxY = if (showWithCones || PreferenceManager.getDefaultSharedPreferences(context)
						.getBoolean("pref_cones_in_bounds", false)
		) maxYWithCones else maxY
		val chartSpace = 0.1f * (realMaxY - minY)
		mChart.axisLeft.axisMaximum = realMaxY + chartSpace
		mChart.axisLeft.axisMinimum = minY - chartSpace
		mChart.data = lineData
		mChart.xAxis.axisMinimum = 0.0f
		mChart.xAxis.axisMaximum = idx
		mChart.visibility = if (mTimes.isEmpty()) View.INVISIBLE else View.VISIBLE
		mChart.notifyDataSetChanged()
		mChart.invalidate()
	}

	private fun makeRunsDataset(
			times: List<RunInfo>,
			idx: Float,
			txtColor: Int,
			circleColor: Int,
			bestRaw: Boolean,
			bestCones: Boolean
	): LineDataSet {
		val entries: ArrayList<Entry> = ArrayList()
		for ((index, data) in times.withIndex()) {
			entries.add(
					Entry(
							idx + index,
							data.run.rawTime(if (showWithCones) mCourseInfo.penVal() else 0.0f),
							RunInfoHelper(data, mCourseInfo.penVal(), bestRaw, bestCones, printThPart)
					)
			)
		}

		val dataSet = LineDataSet(entries, "")
		dataSet.valueTextColor = txtColor
		dataSet.valueTextSize =
				if (times.count() <= 10) 15.0f else if (times.count() > 20) 9.0f else 20.0f - times.count() / 2
		dataSet.setCircleColor(ContextCompat.getColor(requireContext(), R.color.colorGold))
		dataSet.valueFormatter = ChartDateValueFormatter()
		dataSet.valueTypeface = mChartTypeface
		dataSet.setCircleColor(circleColor)
		dataSet.setDrawHorizontalHighlightIndicator(false);
		dataSet.setDrawVerticalHighlightIndicator(false)

		return dataSet
	}

	inner class RunInfoHelper(
			ri: RunInfo,
			conePenalty: Float,
			isBest: Boolean,
			isBestCones: Boolean,
			printThPart: Boolean
	) {
		private var rawTime = ri.run.tm().toFloat()
		private var rawTimeCones = rawTime + ri.run.cones * conePenalty
		var mRun = ri
		var best = isBest
		var bestCones = isBestCones
		val mPen = conePenalty
		val pPrintThPart = printThPart

		fun toString(countCones: Boolean): String {
			return mRun.run.toString(countCones, mPen, if (printThPart) 1 else 0)
		}
	}

	private val mCharDateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")

	inner class ChartDateValueFormatter : ValueFormatter() {
		override fun getPointLabel(entry: Entry?): String {
			val ri: RunInfoHelper = entry?.data as RunInfoHelper
			var tmStr = ri.toString(showWithCones)
			if ((ri.best || ri.bestCones) && ri.mRun.run.created.toLocalDate() != mDate) {
				tmStr = tmStr + "\n" + ri.mRun.run.created.toLocalDate().format(mCharDateFormatter)
			}

			val label = ri.mRun.label()
			return if (label != null) tmStr + "\n" + getFormattedLabel(label) else tmStr
		}

		private fun getFormattedLabel(label: Label): String {
			val ln = label.data.split("\n")[0]
			return if (ln.length > 6) ln.substring(0, 4) + ".." else ln
		}
	}
}
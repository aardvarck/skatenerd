package com.bess.skatenerd

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.databinding.FragmentShowCourseListElementBinding
import com.bess.skatenerd.dbModel.RunInfo
import org.threeten.bp.format.DateTimeFormatter

class ShowCourseRunsRecyclerViewAdapter(
		private val mValues: List<RunInfo>, private val headers: List<String>,
		private val countCones: Boolean, private val conePen: Float, private val printThPart: Boolean
) : RecyclerView.Adapter<ShowCourseRunsRecyclerViewAdapter.ViewHolder>() {

	private val mFormatter = DateTimeFormatter.ofPattern("HH:mm")

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val binding = FragmentShowCourseListElementBinding.inflate(LayoutInflater.from(parent.context), parent, false)
		return ViewHolder(binding)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val run = mValues[position]

		holder.mTimeView.text = mFormatter.format(run.run.created)
		holder.mResultView.text =
				run.run.toString(countCones, conePen, if (printThPart) 1 else 0, false)
		holder.mConesView.text = run.run.cones.toString()

		holder.binding.root.tag = run

		val label = run.label()
		if (label != null) holder.mNoteView.text = label.data else holder.mNoteView.visibility =
				GONE
	}

	override fun getItemCount(): Int = mValues.size

	inner class ViewHolder(val binding: FragmentShowCourseListElementBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
		val mTimeView: TextView = binding.time
		val mResultView: TextView = binding.result
		val mConesView: TextView = binding.cones
		val mNoteView: TextView = binding.note

		init {
			binding.root.setOnClickListener(this)
		}

		override fun onClick(v: View) {
			val ri = v.tag as RunInfo
			v.findNavController().navigate(
					ShowCourseFragmentDirections.actionShowCourseFragmentToRunNotePopupFragment(
							ri.run.id,
							mResultView.text.toString(),
							ri.label()?.id ?: -1,
							ri.label()?.data,
							ri.run.created.toLocalDate(),
							ri.run.timecourse
					)
			)
		}
	}
}

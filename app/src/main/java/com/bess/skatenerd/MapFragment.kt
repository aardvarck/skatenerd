package com.bess.skatenerd

import android.Manifest
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PERMISSION_DENIED
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import com.bess.skatenerd.databinding.FragmentMapDialogBinding
import com.bess.skatenerd.databinding.FragmentNewSpotBinding
import com.bess.skatenerd.dbModel.Place
import com.bess.skatenerd.dbModel.Spot
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMapClickListener
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions


class MapFragment : Fragment(), OnMapReadyCallback, OnMapClickListener {
	private lateinit var mAddressLabel: TextView
	private lateinit var mMapView: MapView
	private lateinit var mGoogleMap: GoogleMap
	private lateinit var mOkDeleteButton: Button
	private var mCurrentMarker: Marker? = null
	private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient
	private var mSomeSpot: Boolean = false
	private var mChanged: Boolean = false
	private var mCurPlace: Place? = null
	private var mCurSpot: Spot? = null
	private var mBinding: FragmentMapDialogBinding? = null
	private val binding get() = mBinding!!

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		outState.putParcelable("spot", mCurSpot)
		outState.putParcelable("place", mCurPlace)
		outState.putBoolean("changed", mChanged)
		outState.putBoolean("someSpot", mSomeSpot)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		mBinding = FragmentMapDialogBinding.inflate(inflater)
		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		val args = MapFragmentArgs.fromBundle(savedInstanceState ?: requireArguments())
		mSomeSpot = args.someSpot
		mCurPlace = args.place
		mCurSpot = args.spot
		if (savedInstanceState != null)
			mChanged = savedInstanceState.getBoolean("changed")

		mMapView = view.findViewById(R.id.mapView)
		mMapView.onCreate(savedInstanceState);
		mMapView.onResume();
		mMapView.getMapAsync(this);

		mAddressLabel = view.findViewById(R.id.addressText)
		mOkDeleteButton = view.findViewById(R.id.okButton)

		val cancelButton = view.findViewById<Button>(R.id.cancelButton)
		cancelButton.setOnClickListener {findNavController().navigateUp()}

		binding.chooseLocation.visibility = if (mSomeSpot) VISIBLE else GONE
		mAddressLabel.visibility = if (mSomeSpot) VISIBLE else GONE
		mOkDeleteButton.visibility = if (mSomeSpot) VISIBLE else GONE
		cancelButton.visibility = if (mSomeSpot) VISIBLE else GONE

		if(mSomeSpot){
			binding.chooseLocation.text = getString(R.string.setSpotLocation, mCurSpot!!.name)
			if(mCurPlace!= null) {
				ViewCompat.setBackgroundTintList(mOkDeleteButton, AppCompatResources.getColorStateList(requireContext(), R.color.colorDelete))
				mOkDeleteButton.text = getString(R.string.delete)
				updateAddress(mCurPlace!!.lat, mCurPlace!!.lon)
				setOkDeleteButtonListener(true)
			}
			else {
				mOkDeleteButton.isEnabled = false
				setOkDeleteButtonListener(false)
			}
		}

		val requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission(),)
		{ isGranted: Boolean ->
			if (isGranted) {
				// Permission is granted. Continue the action or workflow in your
				// app.
				mGoogleMap.isMyLocationEnabled = true
			} else {
				// Explain to the user that the feature is unavailable because the
				// features requires a permission that the user has denied. At the
				// same time, respect the user's decision. Don't link to system
				// settings in an effort to convince the user to change their
				// decision.
			}
		}

		if (ContextCompat.checkSelfPermission( requireContext(),
						"android.permission.ACCESS_FINE_LOCATION") == PERMISSION_DENIED) {
			requestPermissionLauncher.launch("android.permission.ACCESS_FINE_LOCATION")
		}

		mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext());
	}

	private fun setOkDeleteButtonListener(delete: Boolean){
		if(delete)
			mOkDeleteButton.setOnClickListener {
				val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
				builder.setMessage(getString(R.string.deletePlaceWarning)).setNegativeButton(getString(R.string.Cancel), null)
					.setPositiveButton(getString(R.string.delete)) { _, _ ->
						val result = Bundle().apply {putBoolean("deletePlace", true)}
						setFragmentResult("setSpotMap", result)
						findNavController().popBackStack()
					}.show()
			}
		else
			mOkDeleteButton.setOnClickListener {
				val result = Bundle().apply {putParcelable("place", mCurPlace)}
				setFragmentResult("setSpotMap", result)
				findNavController().popBackStack()
			}
	}


	override fun onMapReady(p0: GoogleMap?) {
		mGoogleMap = p0!!
		mGoogleMap.uiSettings.isZoomControlsEnabled = true
		mGoogleMap.uiSettings.isMyLocationButtonEnabled = true
		/*if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return
		}*/
		mGoogleMap.isMyLocationEnabled = true

		if(mSomeSpot){
			if(mCurPlace != null){
				val pos = LatLng(mCurPlace!!.lat, mCurPlace!!.lon)
				mCurrentMarker = mGoogleMap.addMarker(MarkerOptions().position(pos).flat(true))
				zoomToPos(mCurPlace!!.lat, mCurPlace!!.lon)
			}
		}

		/*val locationResult = if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return
		}
				mFusedLocationProviderClient.lastLocation*/
		val locationResult = mFusedLocationProviderClient.lastLocation
		locationResult.addOnCompleteListener(requireActivity()) { task ->
			if (task.isSuccessful) {
				// Set the map's camera position to the current location of the device.
				if(mCurPlace == null) {
					val lastKnownLocation = task.result
					if (lastKnownLocation != null)
						zoomToPos(lastKnownLocation.latitude, lastKnownLocation.longitude)
				}
			}
		}
		mGoogleMap.setOnMapClickListener(this)
	}

	private fun zoomToPos(lat: Double, lon: Double){
		mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lon),15.0f))
	}

	override fun onMapClick(p0: LatLng?) {
		if(!mSomeSpot || p0 == null) return

		if (mCurrentMarker != null) mCurrentMarker?.remove()
		mCurrentMarker = mGoogleMap.addMarker(MarkerOptions().position(p0).flat(true))
		mOkDeleteButton.isEnabled = true
		mOkDeleteButton.text = getString(R.string.Save)
		ViewCompat.setBackgroundTintList(mOkDeleteButton, AppCompatResources.getColorStateList(requireContext(), R.color.simpleGrey))

		val address = updateAddress(p0.latitude, p0.longitude)
		mCurPlace = Place(0, address, p0.latitude, p0.longitude)
		mChanged = true
		setOkDeleteButtonListener(false)
	}

	private fun updateAddress(lat: Double, lon: Double): String{
		val gc = Geocoder(activity)
		val lst: List<Address> = gc.getFromLocation(lat, lon, 1)
		if (lst.isNotEmpty()) mAddressLabel.text = lst[0].getAddressLine(0)
		return mAddressLabel.text.toString()
	}
}

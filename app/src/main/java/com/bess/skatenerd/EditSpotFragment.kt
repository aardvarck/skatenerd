package com.bess.skatenerd
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.bess.skatenerd.databinding.FragmentNewSpotBinding
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.Place
import com.bess.skatenerd.dbModel.Spot

class EditSpotFragment : Fragment() {
	private var mCurSpot = Spot()
	private var mCurPlace: Place? = null
	private var mSpotChanged = false
	private var mPlaceChanged = false
	private var mBinding: FragmentNewSpotBinding? = null
	private val binding get() = mBinding!!

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val args = EditSpotFragmentArgs.fromBundle(savedInstanceState ?: requireArguments())
		mCurPlace = args.place
		mCurSpot = args.spot ?: Spot()
		if (savedInstanceState != null) {
			mSpotChanged = savedInstanceState.getBoolean("spotChanged")
			mPlaceChanged = savedInstanceState.getBoolean("placeChanged")
		}
	}

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		outState.putParcelable("spot", mCurSpot)
		outState.putParcelable("place", mCurPlace)
		outState.putBoolean("spotChanged", mSpotChanged)
		outState.putBoolean("placeChanged", mPlaceChanged)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		mBinding = FragmentNewSpotBinding.inflate(inflater)
		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		if(mCurSpot.id == 0)
			binding.nameEdit.post {
				binding.nameEdit.requestFocus()
				val imm = binding.nameEdit.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
				//imm.showSoftInput(view.nameEdit, InputMethodManager.SHOW_IMPLICIT)
				imm.toggleSoftInput(0, 0)
			}

		binding.pointOnMapButton.setOnClickListener {
			findNavController().navigate(EditSpotFragmentDirections.actionEditSpotFragmentToMapFragment(true, mCurPlace, mCurSpot))}

		binding.okButton.isEnabled = mSpotChanged
		binding.okButton.setOnClickListener {
			mCurSpot.name = binding.nameEdit.editText!!.text.toString()
			mCurSpot.desc = binding.descEdit.editText!!.text.toString()
			makeDecision(false)
			findNavController().navigateUp()
		}
		binding.cancelButton.setOnClickListener {findNavController().navigateUp()}

		binding.deleteButton.isEnabled = (mCurSpot.id != 0)
		binding.deleteButton.setOnClickListener {
			val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
			builder.setMessage(getString(R.string.deleteSpotWarning)).setNegativeButton(getString(R.string.Cancel), null)
				.setPositiveButton(getString(R.string.delete)) { _, _ ->
					makeDecision(true)
					findNavController().navigateUp()
				}.show()
		}

		val updateMapButton = {
			binding.pointOnMapButton.text = if(mCurPlace != null) mCurPlace!!.desc else getString(R.string.pointAtMap)
		}

		binding.nameEdit.editText!!.setText(mCurSpot.name)
		binding.descEdit.editText!!.setText(mCurSpot.desc)
		updateMapButton()

		val tw : TextWatcher = object: TextWatcher {
			override fun afterTextChanged(s: Editable) { binding.okButton.isEnabled = binding.nameEdit.editText!!.text.isNotEmpty(); mSpotChanged = true}
			override fun beforeTextChanged(s: CharSequence?, a: Int, b: Int, c: Int) {}
			override fun onTextChanged(s: CharSequence?, a: Int, b: Int, c: Int) {}
		}
		binding.nameEdit.editText!!.addTextChangedListener(tw)
		binding.descEdit.editText!!.addTextChangedListener(tw)

		setFragmentResultListener("setSpotMap") { _: String, bundle: Bundle ->
			if(bundle.getBoolean("cancel")) return@setFragmentResultListener
			mCurPlace = bundle.getParcelable<Place>("place")
			updateMapButton()
			binding.okButton.isEnabled = true
			mSpotChanged = true
			mPlaceChanged = true
		}
	}

	private fun makeDecision(delete: Boolean){
		if(delete)
			DBCtrl.get().removeSpot(mCurSpot.id)
		else{
			if(mCurSpot.id == 0)
				DBCtrl.get().addSpot(mCurSpot, mCurPlace)
			else{
				DBCtrl.get().renameSpot(mCurSpot)
				if(mCurPlace != null && mPlaceChanged)
					DBCtrl.get().setSpotPlace(mCurSpot.id, mCurPlace!!)
				else
					DBCtrl.get().removePlaceFromSpot(mCurSpot.id)
			}
		}
	}

}
package com.bess.skatenerd

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.databinding.FragmentCourseRankingRunBinding
import com.bess.skatenerd.databinding.FragmentCourseRankingRunCombinedBinding
import com.bess.skatenerd.dbModel.User
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.*


class CourseRankingRecyclerViewAdapter(
		private var mInitialRuns: List<RunSummInfo>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
	private lateinit var mFullRuns: MutableList<RunSummInfo>
	private lateinit var mDateRuns: List<RunSummInfo>
	private var mColors: MutableMap<Int, Int> = mutableMapOf()
	private var mValues: MutableList<RunItem> = mutableListOf()
	private val TYPE_SINGLE = 0
	private val TYPE_COMBINED = 1
	private var mAllRuns: Boolean = false
	private var mWithCones: Boolean = false
	private var mDate: LocalDate? = null
	private lateinit var mBestRuns: MutableMap<Int, Int>

	init{
		sortByDate()
	}

	fun setup(allRuns: Boolean, withCones: Boolean, date: LocalDate?){
		mAllRuns = allRuns
		mWithCones = withCones

		if(mDate != date){
			mDate = date
			sortByDate()
		}

		sortRuns()
	}

	private fun sortByDate(){
		mDateRuns =
				if(mDate == null) mInitialRuns
				else mInitialRuns.filter { it.run.created.toLocalDate() == mDate }
	}

	private fun sortRuns(){
		val tempRuns  = mDateRuns.sortedBy { if(mWithCones) it.timecones else it.timeraw }
		val bestTime = if(mWithCones) tempRuns.first().timecones else tempRuns.first().timeraw

		mFullRuns = mutableListOf()

		if(mAllRuns){
			mFullRuns = tempRuns.toMutableList()
		}
		else{
			mBestRuns = mutableMapOf()
			val users = mutableListOf<Int>()
			var place = 1
			val usrRuns = mutableMapOf<Int, MutableList<RunSummInfo>>()
			for((cnt, r) in tempRuns.withIndex()){
				val usrId = r.user.serverId!!
				if(usrRuns.containsKey(usrId)) usrRuns[usrId]?.add(r)
				else {
					usrRuns[usrId] = mutableListOf(r)
					mBestRuns[usrId] = place
					place++
					users.add(usrId)
				}
				if(!mAllRuns) r.pos	= cnt + 1
			}
			for((cnt, k) in users.withIndex()){
				mFullRuns.addAll(usrRuns[k]!!)
			}

		}

		for((cnt, c) in mFullRuns.withIndex()) {
			if(mAllRuns) c.pos = cnt
			val diff = (if(mWithCones) c.timecones else c.timeraw) - bestTime
			c.diff = "+" + (diff/1000).toString() + "." + (diff % 1000)
		}

		mValues = mutableListOf()
		mValues.add(RunItem(mFullRuns.first(), mFullRuns.first().user, 0, 0, true, 0, 0))

		var curAdapterIdx = 0
		var curRiderId = mFullRuns[0].user.serverId
		for((cnt, r) in mFullRuns.withIndex()){
			if (cnt == 0) continue
			if(r.user.serverId == curRiderId){
				mValues[curAdapterIdx].lr++
				mValues[curAdapterIdx].single = false
			}
			else{
				mValues.add(RunItem(r, r.user, cnt, cnt, true, cnt, 0))
				curAdapterIdx++
				curRiderId = r.user.serverId
			}
		}

		notifyDataSetChanged()
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
		return if(viewType == TYPE_SINGLE){
			val binding = FragmentCourseRankingRunBinding.inflate(LayoutInflater.from(parent.context), parent, false)
			ViewHolderSingle(binding)
		}
		else {
			val binding = FragmentCourseRankingRunCombinedBinding.inflate(LayoutInflater.from(parent.context), parent, false)
			ViewHolderCombined(binding)
		}
	}

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
		val item = mValues[position]
		when(getItemViewType(position)){
			TYPE_SINGLE ->
				(holder as ViewHolderSingle).initialize(item)
			TYPE_COMBINED ->
				(holder as ViewHolderCombined).initialize(item)
		}
	}

	override fun getItemCount(): Int = mValues.size

	override fun getItemViewType(position: Int): Int {
		return if(mValues[position].single) TYPE_SINGLE else TYPE_COMBINED
	}

	private fun getUserColor(user: User): Int{
		return if(mColors.containsKey(user.serverId)) mColors[user.serverId]!!
			else{
				val rnd = Random()
				val color = Color.argb(255, 200 + rnd.nextInt(30), 210 + rnd.nextInt(30), 230 + rnd.nextInt(25))
				mColors[user.serverId!!] = color
				color
			}
	}

	inner class ViewHolderCombined(private val binding: FragmentCourseRankingRunCombinedBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
		private val formatterYear = DateTimeFormatter.ofPattern("dd LLLL yyyy")
		fun initialize(item: RunItem){
			val posTxt = when(mAllRuns) {
				true ->	(item.fr + 1).toString() + "-" + (item.lr + 1).toString()
				false -> mBestRuns[item.user.serverId].toString()
			}
			binding.pos.text = posTxt
			binding.riderName.text = item.user.shortName()
			binding.time.text = item.run.run.toString(false, 0.1f, 1)
			binding.diff.text = item.run.diff
			binding.date.text = item.run.run.created.format(formatterYear)

			binding.root.setOnClickListener(this)

			itemView.setBackgroundColor(getUserColor(item.user))
		}

		override fun onClick(v: View) {
			if(adapterPosition == -1) return
			val item = mValues[adapterPosition]
			val initPos = adapterPosition

			for ((cnt, c) in (item.fr..item.lr).withIndex()) {
				val r = mFullRuns[c]
				if(mAllRuns)
					mValues.add(adapterPosition + cnt, RunItem(r, r.user, c, c, true, c, 0))
				else{
					val pos = if(cnt == 0) mBestRuns[r.user.serverId]!! else -1
					mValues.add(adapterPosition + cnt, RunItem(r, r.user, c, c, true, c, pos))
				}
			}
			val count = item.lr - item.fr + 1
			notifyItemRangeInserted(initPos, count)
			mValues.removeAt(adapterPosition)
			notifyItemRangeRemoved(adapterPosition, 1)
		}
	}

	inner class ViewHolderSingle(private val binding: FragmentCourseRankingRunBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
		private val formatterYear = DateTimeFormatter.ofPattern("dd LLLL yyyy")
		fun initialize(item: RunItem){
			val posTxt = when(mAllRuns) {
				true -> (item.fr + 1).toString()
				false -> if(item.dispPos == -1) "-" else item.dispPos.toString()
			}
			binding.pos.text = posTxt
			binding.riderName.text = item.user.shortName()
			binding.time.text = item.run.run.toString(false, 0.1f, 1)
			binding.diff.text = item.run.diff
			binding.date.text = item.run.run.created.format(formatterYear)

			binding.root.setOnClickListener(this)

			itemView.setBackgroundColor(getUserColor(item.user))
		}

		override fun onClick(v: View) {
			if(adapterPosition == -1) return
			val item = mValues[adapterPosition]

			var startBound = adapterPosition; var endBound = adapterPosition

			for ((cnt, c) in ((adapterPosition-1) downTo 0 ).withIndex()) {
				if(!mValues[c].single || mValues[c].user != item.user) break
				startBound--
			}
			for ((cnt, c) in (adapterPosition+1 until mValues.size).withIndex()) {
				if(!mValues[c].single || mValues[c].user != item.user) break
				endBound++
			}

			if(startBound == endBound) return

			val initPos = mValues[startBound].pos
			mValues.subList(startBound, endBound + 1).clear()
			notifyItemRangeRemoved(startBound, endBound - startBound + 1)

			mValues.add(startBound, RunItem(mFullRuns[initPos], mFullRuns[initPos].user, initPos, initPos + endBound - startBound, false, 0, 0))
			notifyItemRangeInserted(startBound, 1)
		}
	}

	data class RunItem(var run: RunSummInfo, var user: User, var fr: Int, var lr: Int, var single: Boolean, var pos: Int = 0, var dispPos: Int)
}

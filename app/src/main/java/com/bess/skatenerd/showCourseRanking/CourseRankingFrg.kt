package com.bess.skatenerd.showCourseRanking

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.*
import com.bess.skatenerd.databinding.AlertChooseDateBinding
import com.bess.skatenerd.databinding.FragmentCourseRankingBinding
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.Course
import com.bess.skatenerd.dbModel.Spot
import com.google.android.material.progressindicator.CircularProgressIndicator
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

class CourseRankingFrg : Fragment() {
	private val formatterYear = DateTimeFormatter.ofPattern("dd LLLL \nyyyy")
	private lateinit var mCourse: Course
	private lateinit var mSpot: Spot
	private var mAllRuns: Boolean = true
	private var mWithCones: Boolean = false
	private lateinit var mAdapter: CourseRankingRecyclerViewAdapter
	private var mDate: LocalDate? = null
	private lateinit var mRunsView: RecyclerView
	private lateinit var mLoading: CircularProgressIndicator
	private lateinit var mLoadingDesc: TextView
	private lateinit var mUsersDesc: TextView
	private lateinit var mDateButton: Button
	private lateinit var mRuns: MutableList<RunSummInfo>
	private var mBinding: FragmentCourseRankingBinding? = null
	private val binding get() = mBinding!!

	override fun onSaveInstanceState(outState: Bundle) {
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		mBinding = FragmentCourseRankingBinding.inflate(inflater)
		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		val args = CourseRankingFrgArgs.fromBundle(savedInstanceState ?: requireArguments())
		mCourse = args.course

		val desc = DBCtrl.get().getSpotInfo(mCourse.coursespot).name + ", " + mCourse.name
		binding.courseDesc.text = desc

		binding.conesButton.isSelected = false
		binding.conesButton.setOnClickListener{
			it.isSelected = !it.isSelected
			binding.conesButton.backgroundTintList = ColorStateList.valueOf(
					ContextCompat.getColor(requireContext(),if (it.isSelected) R.color.notSoTransparent else R.color.transparent)
			)
			mWithCones =it.isSelected
			updateAdapter()
		}

		binding.allRuns.isChecked = true
		binding.allRuns.setOnCheckedChangeListener{ _, checked ->
			if(checked) {
				binding.bestRuns.isChecked = !checked
				mAllRuns = checked
				updateAdapter()
			}
		}

		mDateButton = binding.dateButton
		mDateButton.text = getString(R.string.allTime)
		mDateButton.setOnClickListener{ chooseDate() }

		binding.bestRuns.isChecked = false
		binding.bestRuns.setOnCheckedChangeListener{ _, checked ->
			if(checked) {
				binding.allRuns.isChecked = !checked
				mAllRuns = !checked
				updateAdapter()
			}
		}

		mRunsView = binding.runsView
		mLoading = binding.loading
		mLoadingDesc = binding.loadingDesc
		mUsersDesc = binding.users

		updateData()
	}

	private fun updateAdapter(){
		mAdapter.setup(mAllRuns, mWithCones, mDate)
	}

	private fun updateData(){
		HttpHelper.get().getCourseRuns(mCourse.serverId!!){
			mRuns = mutableListOf<RunSummInfo>()
			for(t in it.runs){
				val tRaw = t.s * 1000 + t.ms
				val tCones = tRaw +  mCourse.penalty * t.cones * 100;
				mRuns.add(RunSummInfo(t, it.users[t.userid]!!, tCones, tRaw, 0, ""))
			}
			mAdapter = CourseRankingRecyclerViewAdapter(mRuns)
			updateAdapter()

			with(mRunsView) {
				layoutManager = LinearLayoutManager(context)
				adapter = mAdapter
			}
			mLoading.visibility = GONE
			mLoadingDesc.visibility = GONE
			mRunsView.visibility = VISIBLE
			mUsersDesc.text = getString(R.string.Participants, it.users.size)
		}
	}

	private data class DatePair(var set: MutableSet<Int>, var runs: Int)
	private fun chooseDate(): Boolean {
		val map = mutableMapOf<LocalDate, DatePair>()
		for(r in mRuns){
			val dt = r.run.created.toLocalDate()!!
			val usrId = r.user.serverId!!
			if(!map.containsKey(dt))
				map[dt] = DatePair(mutableSetOf(usrId),1)
			else{
				if(map[dt]!!.set.contains(usrId)) map[dt]!!.runs++
				else{
					map[dt]!!.set.add(usrId)
					map[dt]!!.runs++
				}
			}
		}

		val dtList = mutableListOf<DateInfo>()
		map.keys.toList().sortedDescending().forEach { dtList.add( DateInfo(it, map[it]!!.set.size, map[it]!!.runs) ) }

		val builder = AlertDialog.Builder(requireContext())
		val dialogBinding = AlertChooseDateBinding.inflate(LayoutInflater.from(context));
		builder.setView(dialogBinding.root)

		val alertDialog = builder.create()

		val dateUpdate: (LocalDate?) -> Unit = {
			mDate = it
			updateData()
			alertDialog.cancel()
			mDateButton.text = if(it == null) getString(R.string.allTime) else it.format(formatterYear)
		}

		with(dialogBinding.runList) {
			layoutManager = LinearLayoutManager(context)
			adapter = CourseRankingDatesRecyclerViewAdapter(dtList) {date -> dateUpdate(date) }
		}
		dialogBinding.allTimeButton.setOnClickListener { dateUpdate(null) }
		dialogBinding.cancelButton.setOnClickListener {alertDialog.cancel()}

		alertDialog.show()
		return false
	}
}


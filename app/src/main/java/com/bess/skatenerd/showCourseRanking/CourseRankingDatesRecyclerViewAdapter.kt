package com.bess.skatenerd

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.databinding.AlertChooseDateListBinding
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

data class DateInfo(val date: LocalDate, val num: Int, val runs: Int)

class CourseRankingDatesRecyclerViewAdapter(private val mValues: List<DateInfo>, val mDateClick: (LocalDate) -> Unit
) : RecyclerView.Adapter<CourseRankingDatesRecyclerViewAdapter.ViewHolder>() {
	private val formatterYear = DateTimeFormatter.ofPattern("dd LLLL yyyy")

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val binding = AlertChooseDateListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
		return ViewHolder(binding)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.initialize(mValues[position])
	}

	override fun getItemCount(): Int = mValues.size

	inner class ViewHolder(private val binding: AlertChooseDateListBinding) : RecyclerView.ViewHolder(binding.root) {
		fun initialize(dateInfo: DateInfo) {
			binding.dateText.text = dateInfo.date.format(formatterYear)
			binding.participants.text = itemView.context.getString(R.string.Participants, dateInfo.num)
			binding.runs.text = itemView.context.getString(R.string.runs_count, dateInfo.runs)
			binding.root.setOnClickListener{ mDateClick(dateInfo.date) }
		}
	}
}

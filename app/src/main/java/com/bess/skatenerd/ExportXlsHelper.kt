package com.bess.skatenerd

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.content.FileProvider
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.Course
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.ClipData
import android.os.Environment
import org.apache.poi.ss.usermodel.*
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.usermodel.XSSFCellStyle
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.io.IOException
import kotlin.math.max


class ExportXlsHelper() {

	companion object{
		private fun isExternalStorageReadOnly(): Boolean {
			val externalStorageState = Environment.getExternalStorageState()
			return Environment.MEDIA_MOUNTED_READ_ONLY == externalStorageState
		}

		private fun isExternalStorageAvailable(): Boolean {
			val externalStorageState = Environment.getExternalStorageState()
			return Environment.MEDIA_MOUNTED == externalStorageState
		}

		fun exportCourse(ctx: Context, act: Activity, course: Course, date: LocalDate? = null) {
			val formatterYear = DateTimeFormatter.ofPattern("dd LLLL yyyy")
			val courseDates = if (date != null)
				listOf(date)
			else
				DBCtrl.get().getCourseDates(course.id).map { it.toLocalDate() }
			if (courseDates.isEmpty()) return

			val penalty = 0.1f*course.penalty

			val wb = XSSFWorkbook()
			val sheet = wb.createSheet("Sheet1");
			val fmt = wb.createDataFormat().getFormat("0.00")
			val fnt = wb.createFont().apply { bold = true }

			for(i in 0..4) sheet.createRow(i)

			val spot = DBCtrl.get().getSpotInfo(course.coursespot)
			sheet.createRow(0).createCell(1).apply {
				setCellValue(spot.name + ", " + course.name)
				cellStyle = wb.createCellStyle().apply { setFont(fnt) }
			}

			fun createStyle ( ha: HorizontalAlignment = HorizontalAlignment.CENTER, f: Short? = null, tb: Boolean = false, bb: Boolean = false, rb: Boolean = false ): XSSFCellStyle {
				return wb.createCellStyle().apply {
					setAlignment(ha)
					f?.let { dataFormat = f }
					if (tb) setBorderTop(BorderStyle.MEDIUM)
					if (bb) setBorderBottom(BorderStyle.MEDIUM)
					if (rb) setBorderRight(BorderStyle.MEDIUM)
				}
			}

			val styleRightA = createStyle(ha = HorizontalAlignment.RIGHT)
			val styleLeftA = createStyle(ha = HorizontalAlignment.LEFT, f = fmt)
			val styleDouble = createStyle(f = fmt)
			val styleText = createStyle()
			val styleRightBrd = createStyle(f = fmt, rb = true)
			val styleTopBotBrd = createStyle(f = fmt, tb = true, bb = true)
			val styleTopBotRightBrd = createStyle(f = fmt, tb = true, bb = true, rb = true)

			sheet.getRow(1).createCell(0).apply {
				cellStyle = styleRightA
				setCellValue("Penalty:")
			}
			sheet.getRow(1).createCell(1).apply {
				cellStyle = styleLeftA
				setCellValue(penalty.toDouble())
			}

			val firstRunRow = 5
			var maxRuns = 0
			courseDates.forEachIndexed{ dateIndex, item ->
				val startCol = dateIndex*4
				val runs = DBCtrl.get().getCourseRunsForDate(course.id, item)
				val lastRow = firstRunRow + runs.size

				for(i in 0..3) for (j in 2..4 )
					sheet.getRow(j).createCell(startCol+i).cellStyle =
						if (i == 3) styleTopBotRightBrd else styleTopBotBrd

				sheet.getRow(2).getCell(startCol).setCellValue(item.format(formatterYear)) //.atStartOfDay()
				sheet.addMergedRegion(CellRangeAddress(2, 2, startCol, startCol+3))

				sheet.getRow(3).apply {
					getCell(startCol).setCellValue("Best raw:")
					val rLit = CellReference.convertNumToColString(startCol+1)
					getCell(startCol+1).cellFormula = "MIN(${rLit}${firstRunRow+1}:${rLit}${lastRow})"
					getCell(startCol+2).setCellValue("Best:")
					val bLit = CellReference.convertNumToColString(startCol+3)
					getCell(startCol+3).cellFormula = "MIN(${bLit}${firstRunRow+1}:${bLit}${lastRow})"
				}

				sheet.getRow(4).apply {
					getCell(startCol).setCellValue("#")
					getCell(startCol+1).setCellValue("Raw")
					getCell(startCol+2).setCellValue("Cones")
					getCell(startCol+3).setCellValue("Total")
				}

				runs.forEachIndexed { rIdx, rItem ->
					val curRow = rIdx + firstRunRow
					(sheet.getRow(curRow) ?: sheet.createRow(curRow)).apply {
						createCell(startCol).apply {
							setCellValue(rIdx + 1.0)
							cellStyle = styleText
						}
						createCell(startCol+1).apply {
							setCellValue(rItem.run.rawTime(penalty).toDouble())
							cellStyle = styleDouble
						}
						createCell(startCol+2).apply {
							setCellValue(rItem.run.cones.toDouble())
							cellStyle = styleText
						}
						val rLit = CellReference.convertNumToColString(startCol+1)
						val pLit = CellReference.convertNumToColString(startCol+2)
						createCell(startCol+3).apply {
							cellFormula = "${rLit}${curRow+1}+${pLit}${curRow+1}*B2"
						}
					}
				}
				maxRuns = max(maxRuns, runs.size)
			}
			for(i in firstRunRow until firstRunRow+maxRuns)
				for(j in courseDates.indices)
					sheet.getRow(i).apply {
						val idx = j*4+3
						(getCell(idx) ?: createCell(idx)).apply { cellStyle = styleRightBrd }
					}

			val file = File(ctx.getExternalFilesDir (null), "course.xlsx")
			var fileOutputStream: FileOutputStream? = null
			try {
				fileOutputStream = FileOutputStream(file)
				wb.write(fileOutputStream)
			} catch (e: IOException) {
			} catch (e: Exception) {
			} finally {
				try {
					fileOutputStream?.close()
				} catch (ex: Exception) {
					ex.printStackTrace()
				}
			}

			val contentUri: Uri = FileProvider.getUriForFile(ctx,"com.bess.skatenerd.fileprovider", file)
			val sendIntent: Intent = Intent().apply {
				action = Intent.ACTION_SEND
				type = "text/plain"
				flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
				putExtra(Intent.EXTRA_SUBJECT, "Skatenerd runs")
				putExtra(Intent.EXTRA_STREAM, contentUri)
				clipData = ClipData.newRawUri("", contentUri);
			}
			act.startActivity(Intent.createChooser(sendIntent,"Skatenerd runs table"))
/*
			//TEST//val mimetypes = arrayOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
			//TEST//putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
			val subject = "Some runs data"
			val contentUri: Uri = FileProvider.getUriForFile(ctx,"com.bess.skatenerd.fileprovider", file)
			val sendIntent: Intent = Intent().apply {
				action = Intent.ACTION_VIEW
				flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
				putExtra(Intent.EXTRA_SUBJECT, subject)
				putExtra(Intent.EXTRA_STREAM, contentUri)
				setDataAndType(contentUri, "application/vnd.ms-excel")
			}
			try {
				act.startActivity(sendIntent)
			} catch (e: ActivityNotFoundException) {
				act.startActivity(
					Intent(
						Intent.ACTION_VIEW,
						Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.docs.editors.sheets")
					)
				)
			}*/
		}
	}
}
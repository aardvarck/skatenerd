package com.bess.skatenerd

import android.view.MotionEvent
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.GestureDetector
import android.view.View
import android.view.View.OnTouchListener

abstract class OnSwipeTouchListener : OnTouchListener {

	private val gestureDetector = GestureDetector(GestureListener())

	override fun onTouch(v: View, event: MotionEvent): Boolean {
		return gestureDetector.onTouchEvent(event)
	}

	private inner class GestureListener : SimpleOnGestureListener() {

		private val swipeThreshold = 100
		private val swipeVelocityThreshold = 100

		override fun onDown(e: MotionEvent): Boolean {
			return true
		}

//		override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
//			onTouch(e)
//			return true
//		}


		override fun onFling(
				e1: MotionEvent,
				e2: MotionEvent,
				velocityX: Float,
				velocityY: Float
		): Boolean {
			val result = false
			try {
				val diffY = e2.y - e1.y
				val diffX = e2.x - e1.x
				if (Math.abs(diffX) > Math.abs(diffY)) {
					if (Math.abs(diffX) > swipeThreshold && Math.abs(velocityX) > swipeVelocityThreshold) {
						if (diffX > 0) {
							onSwipeRight()
						} else {
							onSwipeLeft()
						}
					}
				} else {
					// onTouch(e);
				}
			} catch (exception: Exception) {
				exception.printStackTrace()
			}

			return result
		}
	}

	abstract fun onSwipeRight()

	abstract fun onSwipeLeft()
}
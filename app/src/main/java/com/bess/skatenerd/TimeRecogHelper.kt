package com.bess.skatenerd

enum class RecogResType {
	Success,
	Partial,
	Failed
}

data class RecogRun(var s: Int, var ms: Int, var cones: Int, var note: String?)

data class RecogResult(
		val type: RecogResType,
		val time: RecogRun?
)

class TimeRecogHelper {
	companion object {
		fun tryToParse(txt: String?): RecogResult {
			if (txt == null) return RecogResult(RecogResType.Failed, null)
			val parts = txt.split(".")
			if (parts.count() > 1) {
				val secPart = parts[0].trim().toIntOrNull()
				val msecPart = parts[1].trim().toIntOrNull()
				if (secPart != null && msecPart != null) {
					val run = RecogRun(secPart, msecPart, 0, null)

					if (parts.count() > 2) {
						val conesPart = parts[2].trim().toIntOrNull()
						when (conesPart) {
							null -> run.note = parts[2]
							else -> {
								run.cones = conesPart
								if (parts.count() > 3) {
									run.note = parts.subList(3, parts.count()).joinToString { it }
								}
							}
						}
					}
					return RecogResult(RecogResType.Success, run)
				}
			}
			return RecogResult(RecogResType.Failed, null)
		}
	}
}



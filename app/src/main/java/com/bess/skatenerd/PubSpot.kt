package com.bess.skatenerd

import android.graphics.Point
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.bess.skatenerd.databinding.FragmentPubspotBinding
import com.bess.skatenerd.dbModel.Course
import com.bess.skatenerd.dbModel.Place
import com.bess.skatenerd.dbModel.Spot
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class PubSpot : DialogFragment(), OnMapReadyCallback, GoogleMap.OnMapClickListener {
	private lateinit var mMapView: MapView
	private lateinit var mGoogleMap: GoogleMap
	private lateinit var mSpot: Spot
	private lateinit var mPlace: Place
	private lateinit var mCourse: Course
	private var mBinding: FragmentPubspotBinding? = null
	private val binding get() = mBinding!!

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val args = savedInstanceState ?: requireArguments()
		if (args != null) {
			mSpot = args.getParcelable("spot")!!
			mPlace = args.getParcelable("place")!!
			mCourse = args.getParcelable("course")!!
		}
	}

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
	}

	override fun onResume() {
		super.onResume()
		val window: Window = dialog!!.window!!
		val display: Display = window.windowManager.defaultDisplay
		val size = Point()
		display.getSize(size)
		window.setLayout((size.x * 0.95).toInt(), (size.y * 0.95).toInt())
		window.setGravity(Gravity.CENTER)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		mBinding = FragmentPubspotBinding.inflate(inflater)
		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding.spotName.editText!!.setText(mSpot.name)
		binding.spotDesc.editText!!.setText(mSpot.desc)

		binding.publishButton.setOnClickListener {
			findNavController().navigate(PubSpotDirections.actionPubSpotToPubCourse(mSpot, mCourse, mPlace))
		}

		mMapView = view.findViewById(R.id.mapView)
		mMapView.onCreate(savedInstanceState);
		mMapView.onResume();
		mMapView.getMapAsync(this);

		//HttpHelper.getInstance(requireContext(), requireActivity() as MainActivity).getNearbySpots(mPlace.lat, mPlace.lon)

		val queue = Volley.newRequestQueue(requireContext())
		val urlTmp = "http://192.168.0.11:8000/spots_in_radius/%f/%f"
		val url = urlTmp.format(mPlace.lat, mPlace.lon)

		val jsonRequest = JsonArrayRequest( Request.Method.GET, url, null,
			{ response ->
				if(response.length() == 0){
					binding.pubSpotDesc.text = getString(R.string.pubSpotDescNoSiblings)
				}
				else{
					binding.pubSpotDesc.text = "there are some spots nearby"
				}
			},
			{ /*textView.text = "That didn't work!"*/ })

		queue.add(jsonRequest)
	}

	override fun onMapReady(p0: GoogleMap?) {
		mGoogleMap = p0!!
		mGoogleMap.uiSettings.isZoomControlsEnabled = true
		mGoogleMap.uiSettings.isMyLocationButtonEnabled = true
		mGoogleMap.isMyLocationEnabled = true

		val pos = LatLng(mPlace.lat, mPlace.lon)
		mGoogleMap.addMarker(MarkerOptions().position(pos).flat(true))
		zoomToPos(mPlace.lat, mPlace.lon)

		mGoogleMap.setOnMapClickListener(this)
	}

	private fun zoomToPos(lat: Double, lon: Double){
		mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lon), 13.0f))
	}

	override fun onMapClick(p0: LatLng?) {
/*		if(!mSomeSpot || p0 == null) return
		if (mCurrentMarker != null) mCurrentMarker?.remove()
		mCurrentMarker = mGoogleMap.addMarker(MarkerOptions().position(p0).flat(true))
		mOkDeleteButton.isEnabled = true
		mOkDeleteButton.text = getString(R.string.Save)
		ViewCompat.setBackgroundTintList(mOkDeleteButton, AppCompatResources.getColorStateList(requireContext(), R.color.simpleGrey))
		val address = updateAddress(p0.latitude, p0.longitude)
		mCurPlace = Place(0, address, p0.latitude, p0.longitude)
		mChanged = true
		setOkDeleteButtonListener(false)*/
	}
}
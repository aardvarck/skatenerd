package com.bess.skatenerd

import android.content.Context
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.User
import com.google.android.gms.auth.api.signin.GoogleSignInAccount

// https://stackoverflow.com/questions/40398072/singleton-with-parameter-in-kotlin
class UserHelper(private val mContext: Context, private val mAct: MainActivity, private val mDbSrc: DBCtrl) {
	companion object {
		@Volatile private var INSTANCE: UserHelper? = null
		fun getInstance(context: Context, act: MainActivity, dbSrc: DBCtrl): UserHelper =
			INSTANCE ?: synchronized(this) {
				INSTANCE ?: buildHelper(context, act, dbSrc).also { INSTANCE = it }
			}
		private fun buildHelper(context: Context, act: MainActivity, dbSrc: DBCtrl) = UserHelper(context, act, dbSrc)
	}
	private var mAcc: GoogleSignInAccount? = null
	private var mClientId: Int = 1
	private var mServerId: Int = 0

	fun signedIn(acc: GoogleSignInAccount?){
		mAcc = acc
		if(mAcc != null && mAcc!!.displayName != null  && mAcc!!.email != null){
			mDbSrc.setUser(User(0, mAcc!!.displayName!!, mAcc!!.email!!, null, true))
		}
	}


}
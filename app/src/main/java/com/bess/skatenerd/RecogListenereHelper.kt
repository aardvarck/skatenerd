package com.bess.skatenerd

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer

interface RecogListenCallbacker {
	fun onResult(txt: String)
	fun onEndOfSpeech()
}

class RecogListenerHelper(
		mCtx: Context,
		val mCallbacker: RecogListenCallbacker
) : RecognitionListener {
	private var speech: SpeechRecognizer
	private var recognizerIntent: Intent

	init {
		speech = SpeechRecognizer.createSpeechRecognizer(mCtx)
		//Log.i(LOG_TAG, "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(this))
		speech.setRecognitionListener(this)

		recognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en")
		recognizerIntent.putExtra(
				RecognizerIntent.EXTRA_LANGUAGE_MODEL,
				RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
		)
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3)
	}

	fun startRecognize() {
		speech.startListening(recognizerIntent)
	}

	override fun onResults(results: Bundle?) {
		val matches = results?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
		val txt = matches?.get(0)
		if (txt != null) mCallbacker.onResult(txt)
	}

	override fun onReadyForSpeech(params: Bundle?) {}
	override fun onRmsChanged(rmsdB: Float) {}
	override fun onBufferReceived(buffer: ByteArray?) {}
	override fun onPartialResults(partialResults: Bundle?) {}
	override fun onEvent(eventType: Int, params: Bundle?) {}
	override fun onBeginningOfSpeech() {}
	override fun onEndOfSpeech() {
		mCallbacker.onEndOfSpeech()
	}

	override fun onError(error: Int) {}
}
package com.bess.skatenerd

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View.GONE
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.bess.skatenerd.db.DBCtrl
import org.threeten.bp.LocalDate
import java.io.Serializable

class RunNoteDialogFragment : DialogFragment() {
	private var mRunId: Int = 0
	private var mNoteId: Int = 0
	private var mRunText: String? = null
	private var mNoteText: String? = null
	private var mCourseId: Int = 0
	private lateinit var mDate: LocalDate
	private var mShowMicroDigit: Boolean = false

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		outState.putInt("runId", mRunId)
		outState.putInt("noteId", mNoteId)
		outState.putInt("courseId", mCourseId)
		outState.putString("runText", mRunText)
		outState.putString("noteText", mNoteText)
		outState.putSerializable("date", mDate as Serializable)
		outState.putSerializable("show_micro_digit", mShowMicroDigit)
	}

	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		val args = RunNoteDialogFragmentArgs.fromBundle(savedInstanceState ?: requireArguments())
		mRunId = args.runId
		mNoteId = args.noteId
		mCourseId = args.courseId
		mRunText = args.runText
		mNoteText = args.noteText
		mDate = args.date
		mShowMicroDigit = args.showMicroDigit

		val builder =
				AlertDialog.Builder(requireActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar)
		val dialogView = LayoutInflater.from(context).inflate(R.layout.fragment_runnote_popup, null)
		builder.setView(dialogView)

		val runView = dialogView.findViewById(R.id.runTxt) as TextView
		runView.text = getString(R.string.run_desc, mRunText)

		val noteView = dialogView.findViewById(R.id.noteTxt) as TextView
		when (mNoteId) {
			0 -> noteView.visibility = GONE
			else -> noteView.text = mNoteText
		}

		val editButton = dialogView.findViewById(R.id.editButton) as Button
		editButton.setOnClickListener {
			val ri = DBCtrl.get().getRun(mRunId)!!
			findNavController().navigate(
					RunNoteDialogFragmentDirections.actionRunNotePopupFragmentToAddTimeFragment(
							ri.run.timecourse,
							ri.run.created.toLocalDate(),
							ri.run.s,
							ri.run.ms,
							false,
							false,
							ri.run.cones,
							mRunId,
							mShowMicroDigit
					)
			)
		}

		val deleteButton = dialogView.findViewById(R.id.deleteButton) as Button
		deleteButton.setOnClickListener {
			DBCtrl.get().removeRun(mRunId)
			findNavController().navigate(
					RunNoteDialogFragmentDirections.actionRunNotePopupFragmentToShowCourseFragment(
							mCourseId, mDate
					)
			)
		}

		return builder.create()
	}
}
package com.bess.skatenerd

import android.content.Intent
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount

/*import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.Scopes
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.drive.Drive*/

class SettingsFragment : PreferenceFragmentCompat() {
	private lateinit var mLogInBtn: Preference
	private lateinit var mLogOffBtn: Preference

	override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
		addPreferencesFromResource(R.xml.preferences)

		val chooseFileForBackup = { pref: String, type: String, code: Int ->
			val btn: Preference? = findPreference(pref)
			btn?.onPreferenceClickListener = Preference.OnPreferenceClickListener {
				val intent = Intent(type).apply {
					addCategory(Intent.CATEGORY_OPENABLE)
					setType("*/*");
					putExtra(Intent.EXTRA_TITLE, "skatedb")
				}
				activity?.startActivityForResult(intent, code)
				true
			}
		}
		chooseFileForBackup("pref_do_backup", Intent.ACTION_CREATE_DOCUMENT, RC_SAVE_DB)
		chooseFileForBackup("pref_load_backup", Intent.ACTION_OPEN_DOCUMENT, RC_LOAD_DB)

		/* //TODO
		SignHelper.get().setListener { acc -> updateSignStatus(acc) }

		mLogInBtn = findPreference("pref_sign_in")!!
		mLogInBtn.onPreferenceClickListener = Preference.OnPreferenceClickListener {
			SignHelper.get().singIn()
			true
		}

		mLogOffBtn = findPreference("pref_sign_off")!!
		mLogOffBtn.onPreferenceClickListener = Preference.OnPreferenceClickListener {
			SignHelper.get().signOut()
			//SignHelper.getInstance().signOut { updateSignStatus() }
			true
		}

		updateSignStatus(GoogleSignIn.getLastSignedInAccount(requireContext()))*/
	}

	fun updateSignStatus(acc: GoogleSignInAccount?){
		mLogInBtn.isVisible = acc == null
		mLogOffBtn.isVisible = acc != null
		if (acc != null) mLogOffBtn.title = getString(R.string.pref_logOff, acc.displayName)
	}

/*    // GOOGLE DRIVE
	fun googleDriveTest(){
		val mAccount = GoogleSignIn.getLastSignedInAccount(context)
		val credential = GoogleAccountCredential.usingOAuth2(context, listOf(Scopes.DRIVE_FILE))
		credential.setSelectedAccount(mAccount?.getAccount())
		val googleDriveService = Drive.Builder(AndroidHttp.newCompatibleTransport(), GsonFactory(), credential)
			.setApplicationName("Your app name").build()
	}*/
}

 package com.bess.skatenerd

import android.content.Context
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.User
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task

interface OnSignInListener{
	fun signedIn(acc: GoogleSignInAccount?)
}

// https://stackoverflow.com/questions/40398072/singleton-with-parameter-in-kotlin
class SignHelper(private val mContext: Context, private val mAct: MainActivity, private val mDbSrc: DBCtrl) {
	companion object {
		@Volatile private var INSTANCE: SignHelper? = null
		fun createInstance(context: Context, act: MainActivity, dbSrc: DBCtrl): SignHelper =
			INSTANCE ?: synchronized(this) {INSTANCE ?: buildHelper(context, act, dbSrc).also { INSTANCE = it }}
		private fun buildHelper(context: Context, act: MainActivity, dbSrc: DBCtrl) = SignHelper(context, act, dbSrc)
		fun get(): SignHelper{return INSTANCE!!}
	}

	public var mUserSrvId = 0
	public var mPublishEnable = false
	private val mGoogleClient: GoogleSignInClient
	private var mSignInListeners = mutableListOf<OnSignInListener>()
	private var mSignInListener: OnSignInListener? = null
	private var mSignInListener2: ((GoogleSignInAccount?) -> Unit)? = null

	init{
		val clientId = mContext.getString(R.string.server_client_id)
		val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestIdToken(clientId).requestEmail().build()
//			.requestScopes(Scope(Scopes.DRIVE_FILE),Scope(Scopes.DRIVE_APPFOLDER))
		mGoogleClient = GoogleSignIn.getClient(mAct, gso);
	}

	fun setListener(listener: (GoogleSignInAccount?) -> Unit){
		mSignInListener2 = listener
	}

	fun addListener(listener: OnSignInListener){
		mSignInListeners.add(listener)
	}

	fun singIn() {
		startActivityForResult(mAct, mGoogleClient.signInIntent, RC_SIGN_IN, null);
	}

	fun signOut(listener: OnCompleteListener<Void>) {
		//val task = mGoogleClient.signOut().addOnCompleteListener(mAct, listener)
		mGoogleClient.signOut().addOnCompleteListener(mAct){ updateSignStatus() }
	}

	fun signOut() {
		mGoogleClient.signOut().addOnCompleteListener(mAct){ updateSignStatus() }
	}

	@Suppress("UNCHECKED_CAST")
	fun <F : Fragment> getFragment(fragmentClass: Class<F>): F? {
		val navHostFragment = mAct.supportFragmentManager.fragments.first() as NavHostFragment
		navHostFragment.childFragmentManager.fragments.forEach {
			if (fragmentClass.isAssignableFrom(it.javaClass)) {
				return it as F
			}
		}
		return null
	}

	fun updateSignStatus(){
		signedIn(GoogleSignIn.getLastSignedInAccount(mContext))
	}

	//fun trySilentSignIn(listener: (GoogleSignInAccount?) -> Unit){
	fun trySilentSignIn(){
		val task: Task<GoogleSignInAccount> = mGoogleClient.silentSignIn()
		if (task.isSuccessful) {
			// There's immediate result available.
            //listener(task.result)
			signedIn(task.result)
		} else {
			// There's no immediate result ready, displays some progress indicator and waits for the
			// async callback.
			//showProgressIndicator()
			task.addOnCompleteListener { task ->
				try {
					//hideProgressIndicator()
					val signInAccount = task.getResult(ApiException::class.java)
					signedIn(signInAccount)
					//listener(task.result)
                    //updateViewWithAccount(account)
				} catch (apiException: ApiException) {
					// You can get from apiException.getStatusCode() the detailed error code
					// e.g. GoogleSignInStatusCodes.SIGN_IN_REQUIRED means user needs to take
					// explicit action to finish sign-in;
					// Please refer to GoogleSignInStatusCodes Javadoc for details
					//updateButtonsAndStatusFromErrorCode(apiException.statusCode)
				}
			}
		}
	}

	private fun signedIn(acc: GoogleSignInAccount?){
		if(acc != null && acc.displayName != null  && acc.email != null) {
			val user = User(0, acc.displayName!!, acc.email!!, null, true)
			mDbSrc.setUser(user)
			HttpHelper.get().signIn(user, acc.idToken!!){ id ->
				mDbSrc.setUserSrvId(acc!!.email!!, id)
				mUserSrvId = id
				mPublishEnable = true
			}
		}
		else {
			mDbSrc.setDefaultUser()
			mUserSrvId = 0
			mPublishEnable = false
		}

		mSignInListener2?.let { it(acc) }
	}
}
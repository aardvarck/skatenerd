package com.bess.skatenerd

import com.bess.skatenerd.dbModel.*
import org.threeten.bp.LocalDateTime

data class SpotSummInfo(var spot: Spot, var place: Place?)
data class CourseSummInfo(var course: Course, var countDesc: String, var dateDesc: String, var lastDate: LocalDateTime)
data class SpotWithCoursesSumm( val spot:SpotSummInfo, val courses: List<CourseSummInfo>, val lastDate: LocalDateTime )

data class RunSummInfo(var run: Run, var user: User, var timecones: Int, var timeraw: Int, var pos: Int = 0, var diff: String)
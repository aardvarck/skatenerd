package com.bess.skatenerd

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import com.github.mikephil.charting.animation.ChartAnimator
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider
import com.github.mikephil.charting.renderer.LineChartRenderer
import com.github.mikephil.charting.utils.ViewPortHandler

class LineChartMultiline : LineChart {
	constructor(context: Context) : super(context) {}
	constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
	constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
			context,
			attrs,
			defStyle
	)

	override fun init() {
		super.init()
		mRenderer = LineChartRendererMultiline(this, mAnimator, mViewPortHandler)

		description.text = ""
		axisLeft.setDrawLabels(false)
		axisLeft.setDrawGridLines(false)
		axisLeft.setDrawAxisLine(false)
		axisRight.setDrawGridLines(false)
		axisRight.setDrawLabels(false)
		axisRight.setDrawAxisLine(false)
		xAxis.setDrawLabels(false)
		xAxis.setDrawGridLines(false)
		xAxis.setDrawAxisLine(false)
		legend.isEnabled = false
		setDrawBorders(false)
	}
}

class LineChartRendererMultiline(
		chart: LineDataProvider?, animator: ChartAnimator?, viewPortHandler: ViewPortHandler?
) : LineChartRenderer(chart, animator, viewPortHandler) {

	override fun drawValue(c: Canvas, valueText: String, x: Float, y: Float, color: Int) {
		mValuePaint.color = color
		val lines = valueText.lines()
		var offset = -mValuePaint.textSize * (lines.count() - 1)
		for (line in lines) {
			c.drawText(line, x, y + offset, mValuePaint)
			offset += mValuePaint.textSize
		}
	}
}
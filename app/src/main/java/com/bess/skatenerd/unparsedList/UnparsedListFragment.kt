package com.bess.skatenerd

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle
import androidx.appcompat.app.AlertDialog
import com.bess.skatenerd.db.DBCtrl
import com.bess.skatenerd.dbModel.Unparsed


class UnparsedListFragment : Fragment(), ItemClickListener<UnparsedInfo> {

	private lateinit var mCourseList: List<Pair<Int, String>>
	private lateinit var mCourseNamesList: List<String>
	private lateinit var mRecView: RecyclerView
	private lateinit var mUnparsedMap: MutableMap<Int, Unparsed>

	override fun onCreateView(
			inflater: LayoutInflater,
			container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		val view = inflater.inflate(R.layout.fragment_unparsed_list, container, false)

		mRecView = view.findViewById(R.id.unparsedList)

		populateData()

		return view
	}

	private fun populateData() {
		val lst = DBCtrl.get().getAllUnparsed().sortedByDescending { it.created }
		val crsMap = mutableMapOf<Int, String>()
		for (crs in DBCtrl.get().getAllCourses()) {
			val crsInf = DBCtrl.get().getCourseInfo(crs.id)
			crsMap[crs.id] = crsInf.spot().name + ", " + crs.name
		}

		mCourseList = crsMap.toList().sortedBy { (_, value) -> value }
		mCourseNamesList = mCourseList.map { it.second }

		val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)

		mUnparsedMap = mutableMapOf<Int, Unparsed>()
		for (unp in lst) {
			mUnparsedMap[unp.id] = unp
		}

		val unparsedList = lst.map {
			UnparsedInfo(
					formatter.format(it.created) + if (it.unparsedcourse != null) " " + crsMap[it.unparsedcourse] else "",
					it.data, it.id, it.unparsedcourse
			)
		}
		mRecView.adapter = UnparsedListRecyclerViewAdapter(unparsedList, this)
	}

	override fun onItemClicked(data: UnparsedInfo) {
		val builder = AlertDialog.Builder(this.context!!)
		val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_unparsed_popup, null)
		builder.setView(dialogView)

		val alertDialog = builder.create()

		val courseList = dialogView.findViewById(R.id.courseList) as ListView
		when (data.courseId) {
			null -> {
				val arrAdapter = ArrayAdapter(
						this.context!!,
						android.R.layout.simple_list_item_1,
						mCourseNamesList
				)
				courseList.setOnItemClickListener(object : AdapterView.OnItemClickListener {
					override fun onItemClick(
							parent: AdapterView<*>?,
							view: View?,
							position: Int,
							id: Long
					) {
						val crsId = mCourseList[position].first
						DBCtrl.get().setUnparsedCourse(data.id, crsId)
						alertDialog.cancel()
						populateData()
					}
				})
				courseList.adapter = arrAdapter
			}
			else -> {
				courseList.visibility = GONE
				val chooseView =
						dialogView.findViewById(R.id.unparsedChooseCourseTextView) as TextView
				chooseView.visibility = GONE
			}
		}

		val deleteUnparsed = dialogView.findViewById(R.id.deleteButton) as Button
		deleteUnparsed.setOnClickListener {
			DBCtrl.get().removeUnparsed(mUnparsedMap[data.id]!!)
			alertDialog.cancel()
			populateData()
		}

		alertDialog.show()
	}
}

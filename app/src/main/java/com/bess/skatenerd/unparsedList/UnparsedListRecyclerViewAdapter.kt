package com.bess.skatenerd

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bess.skatenerd.databinding.FragmentUnparsedElementBinding

interface ItemClickListener<ListData> {
	fun onItemClicked(data: ListData)
}

data class UnparsedInfo(val dataCourse: String, val text: String, val id: Int, val courseId: Int?)

class UnparsedListRecyclerViewAdapter<ListData>(
		private val mValues: List<UnparsedInfo>, val mClickListener: ItemClickListener<ListData>
) : RecyclerView.Adapter<UnparsedListRecyclerViewAdapter<ListData>.ViewHolder>() {

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val binding = FragmentUnparsedElementBinding.inflate(LayoutInflater.from(parent.context), parent, false)
		return ViewHolder(binding)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.mDateCourseView.text = mValues[position].dataCourse
		holder.mTextView.text = mValues[position].text
		holder.binding.root.tag = mValues[position]
	}

	override fun getItemCount(): Int = mValues.size

	inner class ViewHolder(val binding: FragmentUnparsedElementBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
		val mDateCourseView: TextView = binding.dateCourseText
		val mTextView: TextView = binding.unparsedText

		init {
			binding.root.setOnClickListener(this)
		}

		override fun onClick(v: View) {
			mClickListener.onItemClicked(v.tag as ListData)
		}
	}
}
